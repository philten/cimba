# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.
import os, pickle
from math import sin, cos, log, exp, pi
from copy import deepcopy as copy
from .utils import sqrt, find
from .interpolate import Sample2D, PDF

###############################################################################
class GridError(Exception):
    """
    Simple exception for the 'grid' method.
    """
    pass

###############################################################################
def grid(path):
    """
    Load an interpolation grid.

    The grid is searched for along these paths in the following order:
    (0) The absolute path, if the absolute path is given.
    (1) The current directory within the Python interpreter.
    (2) The paths defined by the environment variable 'CIMBA_DATA_PATH'.
    (3) The CIMBA package directory.
    """
    try:
        with open(find(path), "rb") as pkl: dct = pickle.load(pkl)
        return dct
    except: raise GridError(
            "The grid file '%s' does not exist." % path)

###############################################################################
class ParticleGunError(Exception):
    """
    Simple exception for the 'ParticleGun' class.
    """
    pass

###############################################################################
class ParticleGun:
    """
    Class to generate a single particle.

    sigma: cross-section for the sampled particle in millibarn.
    pid:   PDG ID for the sampled particle.

    The following members are used internally but should not normally
    need to be accessed by the user.

    rng:      random number generator function which when called returns 
              a uniform distribution, e.g. 'random.random()'.
    gen:      2D sampler for rho and eta.
    rhoK:     k-parameter for converting rho to pT, e.g. 1/(pT + 1 - min(pT))^k.
    rhoPtMin: minimum pT-parameter for converting rho to pT.
    """
    ###########################################################################
    def __init__(self, grid, path, rng,
                 ptlim = (None, None), etalim = (None, None),
                 rhobin = "center", etabin = "center"):
        """
        Initialize the particle gun.

        grid:   dictionary of the sampling grids.
        path:   path to the sampling grid, e.g. 'all/221'.
        rng:    uniform randum number generator function, e.g. 'rng()'.
        ptlim:  tuple of (min(pT), max(pT)) defining the PDF pT-range in GeV.
        etalim: tuple of (min(eta), max(eta)) defining the PDF eta-range.
        rhobin: keyword of what rho-values should be used to define the PDF,
                'lower', 'center', or 'upper'.
        etabin: keyword of what eta-values should be used to define the PDF.
        """
        # Load the grids.
        try:
            self.sigma = grid["sigmaGen"]/float(grid["nAcc"])
            self.rhoK = float(grid["rhoK"])
            self.rhoPtMin = float(grid["rhoPtMin"])
        except: raise ParticleGunError("The provided grid is malformed.")
        xlim = [None if pt == None else
                1./(pt + 1 - self.rhoPtMin)**self.rhoK for pt in ptlim]
        xlim.reverse()
        ylim = etalim
            
        # Load the particle grid.
        paths = path.split("/")
        self.pid = int(paths[-1])
        try:
            for path in paths: grid = grid[path]
        except: raise ParticleGunError(
                "The path '%s' does not exist." % path)

        # Build the binning.
        try:
            xs, ys, zs = copy(grid["xs"]), copy(grid["ys"]), copy(grid["zs"])
            n, dy = grid["n"], ys[1] - ys[0]
        except: raise ParticleGunError(
                "The grid '%s' does not have 'xs', 'ys', 'zs', or 'n'." % path)
        for vs, vbin in [(xs, rhobin), (ys, etabin)]:
            if vbin.lower() == "lower": del vs[-1]
            elif vbin.lower() == "upper": del vs[0]
            else:
                for i in range(0, len(vs) - 1): vs[i] = (vs[i] + vs[i + 1])/2.
                del vs[-1]

        # Fix the rho endpoints (0 at 0, linear extrapolation at 1).
        if xs[0] > 0:
            for zrow in zs: zrow.insert(0, 0.)
            xs.insert(0, 0.)
        if xs[-1] < 1:
            for zrow in zs:
                zrow += [max(0., (zrow[-1] - zrow[-2])/(
                    xs[-1] - xs[-2])*(1. - xs[-2]) + zrow[-2])]
            xs += [1.]
            
        # Create the sampler (account for x-axis weight in cross-section).
        self.rng = rng
        self.gen = Sample2D(xs, ys, [PDF(xs, z) for z in zs],
                            xlim = xlim, ylim = ylim)
        self.sigma *= n*self.gen.icdf.xdif

    ###########################################################################
    def __call__(self):
        """
        Return a generated three-momentum for a particle of the form
        (x-momentum, y-momentum, z-momentum, weight).
        """
        if self.sigma == 0: raise ParticleGunError(
                "Cannot generate events with cross-section of zero.")
        rho, eta, wgt = self.gen(self.rng(), self.rng())
        pt, phi = rho**(-1./self.rhoK) - 1 + self.rhoPtMin, self.rng()*2*pi
        eeta = exp(2*eta)
        ct = (eeta - 1)/(eeta + 1)
        st = sqrt(1 - ct*ct)
        return (pt*cos(phi), pt*sin(phi), pt*ct/st, wgt/self.gen.icdf.xdif)

###############################################################################
class PythiaDecayError(Exception):
    """
    Simple exception for the 'PythiaDecay' class.
    """
    pass

###############################################################################
class PythiaDecay:
    """
    Class to decay particles using Pythia 8.
    """

    ###########################################################################
    def __init__(self, cfgs = [], seed = 1):
        """
        Initialize the decay tool.

        cfgs:  list of Pythia 8 configuration files and/or strings.
        seed:  seed used to intialize the Pythia random number generator.
        """
        import pythia8
        self.pythia8 = pythia8
        
        # Create and configure Pythia.
        self.pythia = self.pythia8.Pythia("", False)
        self.pythia.settings.flag("Random:setSeed", True)
        self.pythia.settings.mode("Random:seed", seed)
        self.pythia.readString("Print:quiet = on")
        self.pythia.readString("ProcessLevel:all = off")

        # Load the user configurations.
        for cfg in cfgs:
            if os.path.isfile(cfg): self.pythia.readFile(cfg)
            else: self.pythia.readString(cfg)
        self.pythia.init()

    ###########################################################################
    def __call__(self, pid, px, py, pz):
        """
        Decay the given particle.

        pid: PDG ID of the particle to decay.
        px:  x-momentum of the particle in GeV.
        py:  y-momentum of the particle in GeV.
        pz:  z-momentum of the particle in GeV.
        """
        # Create the particle.
        try: pde = self.pythia.particleData.particleDataEntryPtr(pid)
        except: raise PythiaDecayError("Unknown PID %i." % pid)
        prt = self.pythia8.Particle(pid, 80, 0, 0, 0, 0, 0, 0, px, py, pz, 0, 0)
        prt.tau(self.pythia.particleData.tau0(pid)*self.pythia.rndm.exp())
        self.pythia.event.reset()

        # Try performing decays.
        ntry = 0
        while self.pythia.event.size() <= 2:
            if ntry > 100:
                print(self.pythia.event)
                raise PythiaDecayError("Decay failed 100 times.")

            # Create the event.
            m = pde.mSel()
            e = sqrt(px**2. + py**2. + pz**2. + m**2.)
            prt.m(m); prt.e(e)
            self.pythia.event.append(prt)
            if not self.pythia.next() or self.pythia.event.size() <= 2:
                self.pythia.event.reset()
            ntry += 1
        return self.pythia.event
