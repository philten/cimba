#!/usr/bin/env python
# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.

# Update the system path to find the CIMBA module.
# This assumes that 'generate' is in 'cimba/generate.'
import sys, os, inspect, itertools
sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(
                inspect.getfile(inspect.currentframe()))), "../../"))

# Import CIMBA, ROOT, system, and pickle.
import cimba, ROOT, sys, pickle

# Check the usage.
if len(sys.argv) != 3:
    print("Usage: convert.py <input ROOT file name> <output CIMBA file name>")
    exit(1)
iname, oname = sys.argv[1], sys.argv[2]

# Load the ROOT file.
ifile = ROOT.TFile(iname)
if not ifile.IsOpen():
    print("Error: ROOT file %s cannot be opened." % ifile)
    exit(1)

# Export the file into a dictionary and pickle.
pickle.dump(cimba.utils.export(ifile, {}), open(oname, "wb"))
