# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.
"""
This module contains a set of utilities to be used for submission
of CIMBA grid submission jobs through the batch submission software
Ganga:

https://ganga.readthedocs.io

which is used predominantly by the ATLAS and LHCb experimental
collaborations. Jobs can be submitted to the GRID, but can also be
submitted to local batch systems such as Condor and PBS. Output from
these files is in the format of ROOT files, and is merged with the
'hadd' command which is implemented here in this module in Python.

The general flow of using this module is as follows:

# Start Ganga.
ganga

# Import the module.
import sys, os
sys.path.insert(0, os.getcwd())
import batch

# Submit 10 subjobs with seeds 1 - 10, each with 1000 events, locally.
job = batch.submit(1, 10, 1000, 2212, 2212, 14000, ['cfg/soft.cfg'], 
      local = True)

# Merge the job when finished.
batch.merge(job)

# Upload 'grid.tgz' for GRID generation.
batch.upload('grids.tgz')

# Submit the same job, but to the GRID.
job = batch.submit(1, 10, 1000, 2212, 2212, 14000, ['cfg/soft.cfg'])

# Merge the job when finished.
batch.merge(job)
"""
import os

###############################################################################
def submit(seed1 = 1, seed2 = None, events = 1000, id1 = 2212, id2 = 2212,
           com = 1400, cfgs = ["cfg/soft.cfg"], local = False,
           pkg = "grids.tgz", extdir = "", pkgdir = os.getcwd()):
    """
    Submit a grid generation job.
    
    seed1:    seed to use.
    seed2:    if not provided submit one subjob, otherwise range to this seed.
    events:   number of events to produce per subjob.
    id1:      PDG ID of the first beam.
    id2:      PDG ID of the second beam.
    cfgs:     list of optional configuration files.
    pkg:      name of package containing the necessary libraries.
    local:    run locally or on the grid.
    extdir:   the external GRID path.
    pkgdir:   the base directory containing this package.
    """
    from GangaCore.GPI import LocalFile, DiracFile, File
    from GangaCore.GPI import Job, Local, Dirac, Executable, ArgSplitter

    # Determine the platform.
    platform = None
    with open(os.path.join(pkgdir, "run.sh")) as run:
        for line in run:
            if not "export CMT" in line: continue
            try: platform = line.split("=")[1].split("#")[0].strip()
            except: continue
    if not platform: print("submit: cannot determine the platform.")
    
    # Check configuration files exist.
    for cfg in cfgs:
        if not os.path.isfile(cfg):
            print("submit: configuration file '%s' is missing." % cfg)
            return None
    
    # Set the job arguments and name.
    if seed2 < seed1 or seed2 == None: seed2 = seed1
    args = [["./grids", str(seed), str(events), str(id1), str(id2), str(com)]
            + cfgs for seed in range(seed1, seed2 + 1)]
    name = "%i_%i_%i_%i_%i_%f_" % (seed1, seed2, events, id1, id2, com)
    name += "_".join([os.path.basename(c).replace(".cfg", "") for c in cfgs])
    
    # Submit the job.
    job = Job()
    if local:
        job.backend = Local()
        job.inputfiles = [LocalFile(os.path.join(pkgdir, pkg))]
    else:
        job.backend = Dirac(); 
        job.backend.settings["CPUTime"] = 345600
        job.inputfiles = [DiracFile("LFN:" + os.path.join(extdir, pkg))]
    job.name = name
    job.outputfiles  = [LocalFile("*.root")]
    job.application  = Executable()
    job.application.exe = File(os.path.join(pkgdir, "run.sh"))
    job.application.platform = platform
    job.splitter = ArgSplitter(args = args)
    job.do_auto_resubmit = True
    try: job.submit()
    except: pass
    return job

###############################################################################
def merge(job, remove = False, status = None, outdir = os.getcwd(),
          paranoid = False):
    """
    Merge all subjobs of a job. By default everything is summed,
    including metadata provided as 'TParameter's. However, metadata
    with key names containing 'xs' are averaged, i.e. cross-section
    parameters are averaged (this does not properly account for
    statistics).
    
    job:      the job to merge.
    remove:   flag to remove jobs after merging them.
    status:   status of jobs to consider for merging.
    outdir:   the output directory for the merged jobs.
    paranoid: check every object in the ROOT file for corruption.
    """
    # Loop over the subjobs.
    import ROOT, glob, os, math, collections
    print("merge: merging %i, %s." % (job.id, job.name))
    inputs, times, meta = [], [], collections.OrderedDict()
    if status != None: subs = job.subjobs.select(status = status)
    else: subs = job.subjobs
    for sub in subs:

        # Check ROOT file.
        path = glob.glob(sub.outputdir + "/*.root")
        path = path[0] if len(path) == 1 else None
        if path == None:
            print("merge: excluding %i with no output." % sub.id)
            continue
        tfile = ROOT.TFile.Open(path)
        if not validate(tfile, paranoid):
            print("merge: excluding %i with malformed output." % sub.id)
            tfile.Close()
            continue

        # Read the meta information.
        inputs.append(path)
        times += [(sub.time.backend_final() -
                   sub.time.backend_running()).seconds]
        keys = tfile.GetListOfKeys()
        for key in keys:
            key, obj = key.GetName(), key.ReadObj()
            if obj.Class_Name().startswith("TParameter"):
                if key in meta: meta[key][1] += [obj.GetVal()]
                else: meta[key] = [obj.GetName(), [obj.GetVal()]]
        tfile.Close()

    # Combine (split if bigger than 1000 files).
    date = job.time.new()
    date = "%02i%02i%02i" % (date.year - 2000, date.month, date.day)
    inputs = [inputs[i:i + 1000] for i in xrange(0, len(inputs), 1000)]
    output = os.path.join(outdir, date + "." + job.name + ".root")
    if len(inputs) == 1: hadd(output, inputs[0], "-f")
    elif len(inputs) > 1:
        outputs = []
        for idx, subs in enumerate(inputs):
            outputs += [output + ".%i" % idx]
            hadd(outputs[-1], subs, "-f")
            tfile = ROOT.TFile.Open(outputs[-1], "UPDATE")
            tfile.Delete("info;*")
            tfile.Close()
        hadd(output, outputs, "-f")
        for path in outputs: os.remove(path)

    # Write the meta information.
    meta["tMean"] = ["tMean", [sum(times)/float(len(times))]]
    meta["tMin"], meta["tMax"] = ["tMin", [min(times)]], ["tMax", [max(times)]]
    tfile = ROOT.TFile.Open(output, "UPDATE")
    for key, (name, vals) in meta.items():
        tfile.Delete("%s;*" % key)
        val = sum(vals) if key.startsiwth("n") else sum(vals)/float(len(vals))
        tparm = ROOT.TParameter(type(val))(name, val)
        tparm.Write(key, ROOT.TObject.kOverwrite)
    tfile.Close()
    if remove: job.remove()

###############################################################################
def validate(tfile, idx = 0, paranoid = False):
    """
    Validate a ROOT file.

    tfile: ROOT file to validate.
    idx:   associated subjob ID for the file to validate.
    """
    if tfile:
        keys = tfile.GetListOfKeys()
        # Check if file is zombie.
        if tfile.IsZombie():
            print("merge: excluding %i with zombie output." % idx)
            tfile.Close(); return False
        # Check if file has keys.
        elif not len(keys):
            print("merge: excluding %i with keyless output." % idx)
            tfile.Close(); return False
        elif paranoid:
            fld, flds = 0, []
            for key in keys:
                if key.IsFolder(): flds += [key]
            while fld < len(flds):
                keys = flds[fld].ReadObj().GetListOfKeys()
                for key in keys:
                    if key.IsFolder(): flds += [key]
                    try:
                        if type(key.ReadObj()) == ROOT.TObject:
                            "merge: excluding %i with malformed object %s." % (
                                idx, key.GetName())
                            tfile.Close(); return
                    except:
                        "merge: excluding %i with malformed object %s." % (
                            idx, key.GetName())
                        tfile.Close(); return
                fld += 1
    else:
        print("merge: excluding %i with malformed output." % idx); return False
    if paranoid: print("merge: including %i with valid output." % idx)
    return True

###############################################################################
def hadd(output, inputs, flags = None, force = False, skip_errors = False,
         reoptimize = False, no_trees = False, max_files = 0, verbosity = 99,
         comp = 1):
    """
    Python equivalent to the 'hadd' command provided by ROOT. Designed
    to allow a direct Python invocation of 'hadd' for long file lists
    that exceeed the maximum shell character limit (typically 68771).

    output: name of the ROOT output file.
    inputs: list of the input ROOT files.
    flags:  string of flags to use, with the same syntax as 'hadd'.

    The following options are the parameters that are typically set
    with the 'flags' string. Note that if the 'flags' string is passed
    then these options can be overwritten.

    force:       bool, force the re-creation of the output file.
    skip_errors: bool, skip files with errors.
    reoptimize:  bool, re-optimize the ROOT output.
    no_trees:    bool, don"t merge the trees.
    max_files:   maximum files to allow open.
    verbosity:   print verbosity.
    comp:        compression level if re-optimized or differing compression
                 levels.
    """
    import ROOT, os
    # Set maximum tree size (replaces 100 GB limit).
    ROOT.TTree.SetMaxTreeSize(9223372036854775807)
    
    # Parse the flags (same syntax as ROOT hadd).
    if type(flags) is str:
        flags = flags.split("-")
        for flag in flags:
            if not flag: continue
            elif flag[0] is "T": no_trees    = True
            elif flag[0] is "k": skip_errors = True
            elif flag[0] is "O": reoptimize  = True
            elif flag[0] is "n":
                flag = flag.split()
                try: max_files = int(flag[1])
                except: print(
                        "hadd: warning, no number of files found after '-n'.")
            elif flag[0] is "v":
                flag = flag.split()
                try: verbosity = int(flag[1])
                except: print("hadd: warning, no verbosity found after '-v'.")
            elif flag[0] is "f":
                force = True
                if len(flag) > 1:
                    try: comp = int(flag[1])
                    except: print(
                            "hadd: warning, invalid compression '-%s'." % flag)
            else: print("hadd: warning, invalid option '-%s'." % flag)

    # Expand paths.
    if not type(output) is str:  
        print("hadd: error, 'output' must be a string."); return
    if not type(inputs) is list: 
        print("hadd: error, 'inputs' must be a list."); return
    for idx, i in enumerate(inputs):
        if not type(i) is str:
            print("hadd: error, non-string input found."); return
        else: inputs[idx] = os.path.expandvars(i)
    output = os.path.expandvars(output)
    if verbosity > 1: print("hadd: info, output is '%s'." % output)

    # Create the file merger.
    ROOT.gSystem.Load("libTreePlayer")
    merger = ROOT.TFileMerger(False, False)
    merger.SetMsgPrefix("hadd")
    merger.SetPrintLevel(verbosity - 1)
    if max_files > 0: merger.SetMaxOpenedFiles(max_files)
    if not merger.OutputFile(output, force, comp):
        print("hadd: error,opening '%s', pass '-f' to force re-creation." 
              % output)
        return

    # Add the inputs.
    for i in inputs:
        if len(i) < 1: continue
        # Indirect files.
        if i[0] is "@":
            try:
                with open(i[1:]) as indirect:
                    for line in indirect:
                        if not merger.AddFile(line):
                            if skip_errors:
                                print("hadd: warning, skipping '%s' from "
                                       "'%s'." % (line, i[1:]))
                            else:
                                print("hadd: error opening '%s' from '%s'." %
                                       (line, i[1:])); return
            except:
                if skip_errors:
                    print("hadd: warning, skipping file '%s'." % i[1:])
                else: print("hadd: error opening '%s'." % i[1:]); return
        # Direct files.
        elif not merger.AddFile(i):
            if skip_errors: print("hadd: warning, skipping file '%s'." % i)
            else: print("hadd: error opening '%s'." % i); return
           
    # Re-optimization.
    if reoptimize: merger.SetFastMethod(False)
    elif merger.HasCompressionChange():
        print("hadd: warning, different input compressions,"
              " merge will be slower.")

    # Perform the merge.
    merger.SetNotrees(no_trees)
    if merger.Merge():
        if verbosity is 1:
            print("hadd: info, merged %i inputs into '%s'." % output)
    elif verbosity is 1:
        print("hadd: info, failed to merge %i inputs into '%s'." % output)

###############################################################################
def upload(src = None, extdir = "", pkgdir = os.getcwd()):
    """
    Upload a file to the GRID.
    
    src:    local file.
    extdir: external GRID path.
    pkgdir: base directory containing this package.
    """
    from os.path import expandvars, expanduser, isfile, join, basename
    from GangaCore.GPI import DiracFile
    from glob import glob
    if src == None: return
    src = expanduser(expandvars(src))
    pkgdir = expanduser(expandvars(pkgdir))
    if not isfile(src): src = join(pkgdir, src)
    lfn = join(extdir, basename(src))
    df = DiracFile(lfn = lfn)
    try: df.remove()
    except: pass
    df = DiracFile(namePattern = src, lfn = lfn, localDir = pkgdir)
    df.put(lfn = lfn, force = True, uploadSE = "CERN-USER")
    return df
