// CIMBA is licensed under the GNU GPL version 2 or later.
// Copyright (C) 2020 Philip Ilten.

// The actual source for custom grid generation.

// Local headers.
#include "grids.h"

//==========================================================================

// The main program.

int main(int argc, char* argv[]) {

  // Configure Pythia.
  Pythia8::Pythia pythia("xmldoc", false);
  Pythia8::Event &event = pythia.event;
  Configuration cfg(pythia, argc, argv);
  if (!cfg.valid) return 1;
  
  // Set the small pT suppression for hard processes.
  Pythia8::SuppressSmallPT suppress;
  bool hard(false);
  const std::map<std::string, Pythia8::Flag> &flags = pythia.settings.
    getFlagMap("SoftQCD:");
  for (std::map<std::string, Pythia8::Flag>::const_iterator
	 flag = flags.begin(); flag != flags.end(); ++flag) {
    if (flag->second.valNow) {hard = false; break;}
  }
  if (hard) pythia.setUserHooksPtr(&suppress);
  
  // The event loop.
  pythia.init();
  std::vector<Pythia8::Particle*> prts;
  int nEvt(pythia.settings.mode("Main:numberOfEvents"));
  for (int iEvt = 0; iEvt < nEvt;) {
    
    // Generate the event.
    if (!pythia.next()) continue;
    else ++iEvt;
    prts.clear();
    
    // Loop over the particles in the event.
    for (int iPrt = 0; iPrt < (int)event.size(); ++iPrt) {
      Pythia8::Particle &prt = event[iPrt];
      int status(prt.statusAbs());
      if (prt.id() == 90) continue;
      if (prt.status() == 201) continue;
      prt = event[prt.iBotCopy()];
      if (prt.status() == 201) continue;
      else prt.status(201);

      // Fill the histogram for all particles.
      double pt(prt.pT());
      if (pt < cfg.rhoPtMin) continue;
      double rho(cfg.rho(pt)), eta(prt.eta());
      hmap::iterator hist = cfg.find(prt.id(), cfg.all);
      hist->second.Fill(rho, eta);

      // Fill the histogram for particles directly from hadronization.
      if (status < 81 || status > 89) continue;
      hist = cfg.find(prt.id(), cfg.had);
      hist->second.Fill(rho, eta);
    }
  }

  // Finalize and return.
  pythia.stat();
  cfg.finalize(pythia);
  return 0;
}

//==========================================================================

// Configuration class.

//--------------------------------------------------------------------------

// The constructor.

Configuration::Configuration(Pythia8::Pythia &pythia, int argc, char* argv[]) {
  
  // Grab the user arguments.
  pythia.settings.mode("Main:numberOfEvents", 0);
  if (argc < 7) {
    std::cout << "Usage: grids <seed> <events> <beam A> <beam B> "
	      << "<COM energy> <cfg file> ... [additional cfgs]\n";
    valid = false;
    return;
  } else valid = true;
  int seed(std::stoi(argv[1])), events(std::stoi(argv[2])),
    idA(std::stoi(argv[3])), idB(std::stoi(argv[4]));
  double ecm(std::stof(argv[5]));

  // Create the bin edges.
  std::vector<double> rhos(25, 0), etas(25, -8);
  for (int iEdge = 1; iEdge < (int)etas.size(); ++iEdge)
    etas[iEdge] = etas[iEdge - 1] + 2*fabs(etas[0])/(etas.size() - 1);
  for (int iEdge = 1; iEdge < (int)rhos.size(); ++iEdge)
    rhos[iEdge] = rhos[iEdge - 1] + 1.0/(etas.size() - 1);

  // Add the CIMBA bin settings.
  pythia.settings.addParm("CIMBA:rhoK", 2, false, false, 0, 0);
  pythia.settings.addParm("CIMBA:rhoPtMin", 0.25, true, false, 0, 0);
  pythia.settings.addPVec("CIMBA:rhoBins", rhos, false, false, 0, 0);
  pythia.settings.addPVec("CIMBA:etaBins", etas, false, false, 0, 0);
  
  // Settings not to be touched by users.
  pythia.settings.mode("Main:numberOfEvents", events);
  pythia.settings.flag("Random:setSeed", true);
  pythia.settings.mode("Random:seed", seed);
  pythia.settings.mode("Beams:idA", idA);
  pythia.settings.mode("Beams:idB", idB);
  pythia.settings.mode("Beams:frameType", 1);
  pythia.settings.parm("Beams:eCM", ecm);
  pythia.settings.flag("ParticleDecays:mixB", false);
  pythia.settings.mode("PhaseSpace:pTHatMinDiverge", 0.5);
  
  // Read the configuration files.
  std::vector<std::string> cfgs;
  for (int iArg = 6; iArg < argc; ++iArg) {
    cfgs.push_back(argv[iArg]);
    pythia.readFile(argv[iArg]);
  }
  pythia.settings.addWVec("Main:cfgs", cfgs);
  
  // Create the template histograms.
  rhos = pythia.settings.pvec("CIMBA:rhoBins");
  etas = pythia.settings.pvec("CIMBA:etaBins");
  hst = TH2D("hst", "", rhos.size() - 1, &rhos[0], etas.size() - 1, &etas[0]);

  // Set the rho parameters.
  rhoK = pythia.settings.parm("CIMBA:rhoK");
  rhoPtMin = pythia.settings.parm("CIMBA:rhoPtMin");
  std::cout << "grids: generating " << events << " events.\n";
}

//--------------------------------------------------------------------------

// Write the histograms to file.

void Configuration::finalize(Pythia8::Pythia &pythia) {

  // Determine the output name.
  std::string cfg, name
    (std::to_string(pythia.settings.mode("Random:seed")) + "_" +
     std::to_string(pythia.settings.mode("Beams:idA")) + "_" +
     std::to_string(pythia.settings.mode("Beams:idB")) + "_" +
     std::to_string(pythia.settings.parm("Beams:eCM")));
  std::vector<std::string> cfgs = pythia.settings.wvec("Main:cfgs");
  for (int iCfg = 0; iCfg < (int)cfgs.size(); ++ iCfg) {
    cfg += (iCfg ? "," : "") + cfgs[iCfg];
    std::string base(cfgs[iCfg].substr(cfgs[iCfg].find_last_of("\\/") + 1));
    name += "_" + base.substr(0, base.find_last_of("."));
  }
  
  // Write the meta info.
  TFile file((name + ".root").c_str(), "RECREATE");
  par(file, "cfg", pythia.settings.mode("Random:seed"), cfg);
  par(file, "rhoK", rhoK);
  par(file, "rhoPtMin", rhoPtMin);
  par(file, "nTry", pythia.info.nTried());
  par(file, "nSel", pythia.info.nSelected());
  par(file, "nAcc", pythia.info.nAccepted());
  par(file, "sigmaGen", pythia.info.sigmaGen());
  par(file, "sigmaErr", pythia.info.sigmaErr());
  
  // Write the histogram output.
  std::cout << "all: " << all.size() << "\n";
  std::cout << "had: " << had.size() << "\n";
  file.mkdir("all");
  file.mkdir("had");
  file.cd("/all");
  for (hmap::iterator hist = all.begin(); hist != all.end(); ++hist)
    hist->second.Write(hist->second.GetName(), TObject::kOverwrite);
  file.cd("/had");
  for (hmap::iterator hist = had.begin(); hist != had.end(); ++hist)
    hist->second.Write(hist->second.GetName(), TObject::kOverwrite);
  
  // Close and return.
  file.Write(0, TObject::kOverwrite);
  file.Close();

}

//--------------------------------------------------------------------------

// Return a histogram iterator.

hmap::iterator Configuration::find(int pid, hmap &hists) {

  hmap::iterator hist = hists.find(pid);
  if (hist == hists.end()) {
    hist = hists.insert(std::make_pair(pid, hst)).first;
    hist->second.SetName(std::to_string(pid).c_str());
  }
  return hist;

}

//--------------------------------------------------------------------------

// Return rho, given pT.

 double Configuration::rho(double pt) {return pow(pt + 1 - rhoPtMin, -rhoK);}

//--------------------------------------------------------------------------

// Write a parameter to a TFile.

void Configuration::par(TFile &file, std::string name, double val,
			std::string title) {

  if (title == "") title = name;
  TParameter<double> tpar(title.c_str(), val);
  tpar.Write(name.c_str(), TObject::kOverwrite);

}

//==========================================================================
