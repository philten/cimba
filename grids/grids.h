// CIMBA is licensed under the GNU GPL version 2 or later.
// Copyright (C) 2020 Philip Ilten.

// Header for the grid generation code.

#ifndef CIMBA_GRIDS_H
#define CIMBA_GRIDS_H

// STL headers.
#include <string>

// ROOT headers.
#include "TFile.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TParameter.h"

// Pythia.
#include "Pythia8/Pythia.h"

// Useful type definitions.
typedef std::map<int, TH2D> hmap;

//==========================================================================

// Configuration class steers the event generation.

class Configuration {

public:
  
  // Constructor.
  Configuration(Pythia8::Pythia &pythia, int argc, char* argv[]);

  // Finalize the configuration.
  void finalize(Pythia8::Pythia &pythia);
  
  // Return a histogram iterator.
  hmap::iterator find(int pid, hmap &hists);

  // Return rho, given pT.
  double rho(double pt);

  // Write a parameter to a TFile.
  void par(TFile &file, std::string name, double val, std::string title = "");
  
  // Members.
  bool valid;
  int rhoK; double rhoPtMin;
  TH2D hst;
  hmap all, had;
};

#endif // CIMBA_GRIDS_H
