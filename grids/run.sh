#!/usr/bin/env bash
# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.

# Script for creating the environment for running interpolation
# grids. The usage is "./run.sh <commands> ...". To create a shell
# with the environemtn, run "./run.sh bash" or use your favourite
# shell flavor.

# Global variables.
export CMT=x86_64-centos7-gcc9-opt                      # CMT version.
export LCG=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b # LCG path.
export GCC=$LCG/gcc/9.1.0/x86_64-centos7                # GCC path.
export PYTHIA=pythia8240                                # Pythia version.
export ROOT=$LCG/ROOT/6.18.04/$CMT                      # ROOT path.
export DAVIX=$LCG/Davix/0.7.3/$CMT                      # DAVIX path.

# Set the compiler and library path.
source $GCC/setup.sh
export LD_LIBRARY_PATH=$LCG/tbb/2019_U7/$CMT/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LCG/vdt/0.4.3/$CMT/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$DAVIX/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$ROOT/lib:$LD_LIBRARY_PATH
    
# Unpack the tarball if needed.
if [ ! -f "grids" ] && [ -f "grids.tgz" ]; then
    tar -xzf grids.tgz --skip-old-files
fi

# Run the arguments.
$@
