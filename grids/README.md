# Interpolation Grid Generation

All the necessary code for generating new interpolation grids is provided here, with the following file structure.

* `README.md`: is this file.
* `Makefile`: rules to build this package using `make`.
* `grids.cc`: C++ source code for grid generation.
* `grids.h`: C++ header code for grid generation.
* `run.sh`: script to setup the correct runtime environment for batch jobs.
* `batch.py`: batch submission routines for Ganga.
* `cfg`: folder with configurations for grid generation.

## Build

The grid generation code, `grids.cc` and `grids.h`, has two dependencies: Pythia 8 and ROOT. Additionally, a C++ compiler which supports the C++11 standard must be used, *i.e.* the option `-std=c++11` is valid. The Pythia 8 dependency is handled automatically with the `Makefile` by downloading and compiling a local version. This version number can be changed with the variable `PYTHIA` in `Makefile`, but by default is 8.240. The ROOT dependency requires `root-config` to be available in the user path, as this is called to configure the build. To build, run the following.
```bash
# Clean the directory.
make clean

# Build the grid binary.
make grids

# Build the entire package for batch deployment.
make grids.tgz
```
The default `make` will build the `grids` binary as well as the `grids.tgz` package for batch system deployment.

When running on distributed batch systems, *e.g.* the GRID, it is necessary to ensure a consistent build is used. The `run.sh` script provides this environment, based on CERN SL6 LXPLUS. Specifically, the architecture `x86_64-centos7-gcc9-opt` is chosen with LCG version `LCG_96b`. The LCG GCC version is `9.1.0` and the LCG ROOT version is `6.18.04`. These can be changed by the global variables at the begining of `run.sh`. To build with this environment run `./run.sh make <targets>` where `<targets>` is the same as before.

## Local Generation

Grid files are genenerated with the `grids` binary. The usage is,
```
./grids <seed> <events> <beam A> <beam B> <COM energy> <cfg file> ... [additional cfgs]
```
where `<beam A>` and `<beam B>` are the PDG IDs of the first and second beam. Some examples follow.
```bash
# Generate a grid for minimum bias events from pp collisions at 14 TeV.
./grids 1 1000 2212 2212 14000 cfg/soft.cfg

# Generate the same grid, but now for hard bbbar events.
./grids 1 1000 2212 2212 14000 cfg/bbbar.cfg
```
If using the environment provided by `run.sh` then all the above commands should be prefaced with `./run.sh`.

## Batch Generation

This file `batch.py` contains a set of utilities to be used for submission of CIMBA grid submission jobs through the batch submission software [Ganga](https://ganga.readthedocs.io) which is used predominantly by the ATLAS and LHCb experimental collaborations. Jobs can be submitted to the GRID, but can also be submitted to local batch systems such as Condor and PBS. Output from these files is in the format of ROOT files, and is merged with the 'hadd' command which is implemented here in this module in Python.

The general flow of using this module is as follows:

```bash
# Start Ganga.
ganga

# Import the module.
import sys, os
sys.path.insert(0, os.getcwd())
import batch

# Submit 10 subjobs with seeds 1 - 10, each with 1000 events, locally.
job = batch.submit(1, 10, 1000, 2212, 2212, 14000, ['cfg/soft.cfg'], local = True)

# Merge the job when finished.
batch.merge(job)

# Upload 'grid.tgz' for GRID generation.
batch.upload('grids.tgz', extdir = '<user path>')

# Submit the same job, but to the GRID.
job = batch.submit(1, 10, 1000, 2212, 2212, 14000, ['cfg/soft.cfg'], extdir = '<user path>')

# Merge the job when finished.
batch.merge(job)
```

## File Conversion

The output of the grid generation is a ROOT file, which is very useful for storing histograms, but would require ROOT to be available in CIMBA. To remove this ROOT dependency in the core CIMBA modules, these ROOT files are converted to Python pickle files that can be easily loaded. Each grid file is stored in a single dictionary, with sub-dictionaries representing sub-directories or attributes of classes. Currently, three types of ROOT classes are handled.
* `TH2`: stored as a dictionary with three elements: `xs` are the x-bin edges, `ys` are the y-bin edges, `zs` are the bin contents, and `n` is the integral of the histogram.
* `TParameter`: is stored simply as its value.
* `TDirectoryFile`: results in a sub-dictionary containing all the objects in that directory.

To convert a ROOT file produced with `grids` into the format used by CIMBA, simply run the following.
```bash
./convert.py <input ROOT filename> <output CIMBA filename>
```
