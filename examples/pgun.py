# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.

# Generate a given particle type and compare to Pythia, if available.

# Update the system path to find the CIMBA module.
# This assumes that 'examples' is in 'cimba/examples.'
import sys, os, inspect, itertools
sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(
                inspect.getfile(inspect.currentframe()))), "../../"))

# Load CIMBA.
import cimba
from cimba.utils import sqrt
from math import log

###############################################################################
def pt(px, py):
    """
    Return pT given a px and py.

    p[x,y]: x and y-components of the three momentum.
    """
    return sqrt(px**2. + py**2.)

###############################################################################
def eta(px, py, pz):
    """
    Return pseudorapidity for a three-momentum.

    p[x,y,z]: x, y, and z-components of the three momentum.
    """
    return (1. if pz > 0. else -1.)*log(
        (sqrt(pt(px, py)**2. + pz**2.) + abs(pz))/max(1e-20, pt(px, py)))

###############################################################################
def generate(name, pid, ncimba = 1e5, npythia = 1e4,
             ptlim = (None, None), etalim = (None, None), 
             grid = "../data/pp14TeV.pkl"):
    """
    Generate and return the pseudorapidity and transverse momentum
    spectrum for a given particle using CIMBA and compare to Pythia if
    available.

    name:      base name for the generating distribution plots.
    pid:       PDG ID of the particle to generate.
    ncimba:    number of particles to generate with CIMBA.
    nythia:    number of events to generate with Pythia.
    ptlim:     particle pT requirement in GeV.
    etalim:    particle eta requirement.
    grid:      path to the grid to use for generation.
    """
    # Create the random number generator.
    import random
    rng = random.Random()
    rng.seed(1)
            
    # Create the particle gun.
    grid = cimba.grid(grid)
    ptlim = [ptlim[0], ptlim[1] if ptlim[1] else 5.]
    pgun = cimba.ParticleGun(grid, "all/" + str(pid), rng.random,
                             ptlim = ptlim, etalim = etalim)
    etalim = [etalim[0] if etalim[0] else pgun.gen.ys[0],
              etalim[1] if etalim[1] else pgun.gen.ys[-1]]
    ptlim[0] = max(ptlim[0], pgun.rhoPtMin)
              
    # Plot the generating distributions.
    pyplot = cimba.utils.pyplot()
    if pyplot:

        # Plot the 2D generation grid.
        dst = grid["all"][str(pid)]
        fig, ax = pyplot.subplots()
        zs = ax.pcolor(dst["xs"], dst["ys"], dst["zs"])
        ax.set_xlim([dst["xs"][0], dst["xs"][-1]])
        ax.set_ylim([dst["ys"][0], dst["ys"][-1]])
        ax.set_xlabel(r"$\rho$")
        ax.set_ylabel(r"$\eta$")
        cb = fig.colorbar(zs)
        cb.set_label(r"PDF($\rho, \eta$)", rotation = 90)
        cimba.utils.logo()
        fig.savefig(name + "_grid.pdf")
        
        # Plot the rho projection.
        fig, ax = pyplot.subplots()
        ax.plot(*cimba.utils.plot(pgun.gen.pdf, [(0, 1)]))
        ax.plot(pgun.gen.pdf.xs[:-1], pgun.gen.pdf.ys, "k.")
        ax.set_xlabel(r"$\rho$")
        ax.set_ylabel(r"PDF($\rho$)")
        cimba.utils.logo()
        fig.savefig(name + "_rho.pdf")
    
    # Create the histograms.
    nbin = 50.0
    deta = (etalim[1] - etalim[0])/(nbin + 1.)
    etas = cimba.Histogram()
    etas.xs = [etalim[0] + i*deta for i in range(0, int(nbin + 2))]
    dpt = (ptlim[1] - ptlim[0])/(nbin + 1.)
    pts = cimba.Histogram()
    pts.xs = [ptlim[0] + i*dpt for i in range(0, int(nbin + 2))]
    hsts = {"eta": {"xs": etas.xs}, "pt": {"xs": pts.xs}}
    
    # Generate with CIMBA.
    hsts["pt"]["cimba"] = [0.]*len(pts.xs)
    hsts["eta"]["cimba"] = [0.]*len(etas.xs)
    tcimba = cimba.utils.Progress(ncimba)
    for itry in range(0, int(ncimba)):
        tcimba.next()
        px, py, pz, wgt = pgun()
        hsts["pt"]["cimba"][pts[pt(px, py)]] += wgt*dpt
        hsts["eta"]["cimba"][etas[eta(px, py, pz)]] += wgt*deta
    for key in hsts:
        ys = hsts[key]["cimba"]
        for idx, y in enumerate(ys): ys[idx] *= pgun.sigma/ncimba
        
    # Run Pythia if available.
    try: import pythia8
    except: pythia8 = None
    if pythia8:
        pythia = pythia8.Pythia("", False)
        pythia.readString("SoftQCD:all = on")
        pythia.init()
        
        # Generate with Pythia.
        hsts["pt"]["pythia"] = [0.]*len(pts.xs)
        hsts["eta"]["pythia"] = [0.]*len(etas.xs)
        tpythia = cimba.utils.Progress(npythia)
        for itry in range(0, int(npythia)):
            tpythia.next()
            if not pythia.next(): continue
            event = pythia.event
            for prt in event:
                if prt.id() != pid: continue
                if not etalim[0] < prt.eta() < etalim[1]: continue
                if not ptlim[0] < prt.pT() < ptlim[1]: continue
                hsts["pt"]["pythia"][pts[prt.pT()]] += dpt
                hsts["eta"]["pythia"][etas[prt.eta()]] += deta
        for key in hsts:
            ys = hsts[key]["pythia"]
            for idx, y in enumerate(ys):
                ys[idx] *= pythia.info.sigmaGen()/npythia

    # Print the timing.
    if pythia8:
        print "%15s %9.2e\n%15s %9.2e\n%15s %9.2e" % (
            "CIMBA/event:", tcimba.time()[1], "PYTHIA/event:",
            tpythia.time()[1], "ratio:", tpythia.time()[1]/tcimba.time()[1])
    
    # Return the histograms.
    return hsts
            
###############################################################################
def plot(name, hsts, xlabel, ylim = (None, None), yscale = None):
    """
    Plot the histogram.

    hsts:   histograms to plot.
    name:   name of the output plot.
    xlabel: label for the x-axis.
    ylim:   limits for the y-axis.
    yscale: scale type for the y-axis.
    """
    pyplot = cimba.utils.pyplot()
    fig, ax = pyplot.subplots()
    for gen, label in zip(["cimba", "pythia"], ["CIMBA", r"\textsc{Pythia} 8"]):
        if not gen in hsts: continue
        else: ys = hsts[gen]
        ax.plot(*cimba.utils.plot((hsts["xs"], ys)), label = label)
        ax.set_xlabel(xlabel)
        if "[" in xlabel: ylabel = r"d$\sigma/$d" + xlabel.replace("[", "[mb/")
        else: ylabel = r"d$\sigma/$d" + xlabel + " [mb]"
        ax.set_ylabel(ylabel)
        ax.set_xlim([hsts["xs"][0], hsts["xs"][-1]])
        if ylim[0] or ylim[1]: ax.set_ylim(ylim)
        if yscale: ax.set_yscale(yscale)
        ax.legend()
        cimba.utils.logo()
        fig.savefig(name)

###############################################################################
if __name__ == "__main__":
    """
    Generate the pT and eta spectra.
    """
    # Run the generation for pions.
    hsts = generate(name = "pgun", pid = 211, ncimba = 1e4, npythia = 1e3,
                    ptlim = (0, 5.))
    plot("pgun_eta.pdf", hsts["eta"], r"$\eta$")
    plot("pgun_pt.pdf", hsts["pt"], r"$p_\mathrm{T}$ [GeV]", yscale = "log")
