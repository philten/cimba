# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.

# Generates the inclusive di-lepton spectrum produced from EM currents
# in hadron decays. This reproduces the di-muon result from the dark
# photon search proposal of arXiv:1603.08926 and the di-electron
# result from the TM search proposal of arXiv:1904.08458. This example
# requires Pythia 8 to perform particle decays.

# Update the system path to find the CIMBA module.
# This assumes that 'examples' is in 'cimba/examples.'
import sys, os, inspect, itertools
sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(
                inspect.getfile(inspect.currentframe()))), "../../"))

# Load the CIMBA and Pythia modules.
import cimba, pythia8

###############################################################################
def fiducial(dtr0, dtr1):
    """
    Define the fiducial region for the dark photon. Returns true if
    the selection is passed.
    
    dtr0: first dark photon decay product four-momentum of type 'Vec4'.
    dtr1: second dark photon decay product four-momentum of type 'Vec4'.
    """
    for dtr in [dtr0, dtr1]:
        if not (2 < dtr.eta() < 5): return False 
        if not (dtr.pAbs() > 10): return False
        if not (dtr.pT() > 0.5): return False
    mom = dtr0 + dtr1
    if not (2 < mom.eta() < 5): return False
    if not (mom.pT() > 1): return False
    return True

###############################################################################
def generate(pidl = 13, ntry = 1e3, bwdh = 0.01, bmin = 0.2, bmax = 1.0,
             ptlim = (1.0, None), etalim = (2., 5.), lumi = 15,
             mres = 0.016, hads = [], sigmamin = 0,
             grid = "data/pp14TeV.pkl"):
    """
    Generate the electromagnetic spectrum for di-leptons using
    CIMBA. Returns a dictionary of histograms which can be plotted
    with the 'plot' method.

    pidl:      final state lepton to use.
    ntry:      number of particles to generate per channel.
    bwdh:      bin width.
    bmin:      minimum bin.
    bmax:      maximum bin.
    ptlim:     hadron pT requirement in GeV.
    etalim:    hadron eta requirement.
    lumi:      luminosity in inverse fb.
    mres:      four times mass resolution in GeV.
    hads:      optional, only generate for hadrons with these PDG IDs.
    sigmamin:  minimum cross-section times branching fraction to run.
    grid:      path to the grid to use for generation.
    """
    # Create the Pythia decayer.
    decay = cimba.PythiaDecay()
    pdb = decay.pythia.particleData
    pdel = pdb.particleDataEntryPtr(pidl)

    # Fix the threshold eta -> pi+ pi- mu+ mu- channel.
    decay.pythia.readString("221:addChannel = 1 0.000000012 12 211 -211 13 -13")
    
    # Turn off all particle decays to avoid double counting.
    pid = 1
    while pid != 0:
        pdb.mayDecay(pid, False)
        pid = pdb.nextId(pid)
    
    # Load the sampling grid.
    grid = cimba.grid(grid)
    
    # Create the histograms.
    hst = cimba.Histogram()
    hst.xs = [bmin + i*bwdh for i in range(0, int((bmax - bmin)/bwdh + 1) + 1)]
    hsts = {"xs": hst.xs}
    
    # Loop over the available particles.
    for pid, pdf in grid["all"].items():
        
        # Require particle is a prompt hadron and can be sampled.
        pid = int(pid)
        if pdf["n"] == 0: continue
        pde = pdb.particleDataEntryPtr(pid)
        if not pde.isHadron(): continue
        if pde.tau0() > 1e-2: continue
        if len(hads) and not pid in hads: continue
        
        # Skip if no available phase-space, e.g. m(hadron) < 2*m(lepton).
        if pde.m0() < 2*pdel.m0(): continue
        
        # Determine the channels to use. Add channels with gamma -> lepton+
        # lepton- if they don't already exist.
        decs = {}
        for idx in range(0, pde.sizeChannels()):
            chn = pde.channel(idx)
            if not chn.bRatio(): continue
            dtrs = [chn.product(j) for j in range(0, chn.multiplicity())]
            dtrs.sort()
            
            # The decay has a lepton pair(s).
            if dtrs.count(pidl) > 0 and dtrs.count(-pidl) > 0:
                decs[tuple(dtrs)] = (idx, chn.bRatio(), chn.meMode())
    
            # The decay has no lepton pairs but has photons with phase-space.
            elif dtrs.count(22) > 0 and (pde.m0() - sum(
                    pdb.m0(dtr) for dtr in dtrs) - 2*pdel.m0() > 0):
    
                # Determine the modified daughters and branching ratio.
                alpha = decay.pythia.couplings.alphaEM(pde.m0())
                br = dtrs.count(22)*alpha*chn.bRatio()
                del dtrs[dtrs.index(22)]
                dtrs += [-pidl, pidl]
                dtrs.sort()
    
                # Determine the matrix element mode.
                # 0:  phase-space.
                # 11: three-body Dalitz decay.
                # 12: n-body Dalitz decay.
                me = chn.meMode()
                if me > 12: me = me
                elif me == 11: me = 12
                else: me = 11 if len(dtrs) == 3 else 12
                
                # Insert the decay if not already included.
                key = tuple(dtrs)
                if not key in decs: decs[key] = (-1, br, me)
    
        # Continue if no channels.
        if len(decs) == 0: continue
        
        # Allow mother to decay.
        pde.setMayDecay(True)
    
        # Sample each decay (use the Pythia random number generator).
        pgun = cimba.ParticleGun(grid, "all/" + str(pid),
                                 decay.pythia.rndm.flat,
                                 ptlim = ptlim, etalim = etalim)
        if pgun.sigma == 0: continue
        for dec, (idx, br, me) in decs.items():
            hsts[(pid, dec)] = [0.]*len(hst.xs)
    
            # Select channel.
            for chn in range(0, pde.sizeChannels()): pde.channel(chn).onMode(0)
            if idx >= 0: pde.channel(idx).onMode(1)
            else:
                dtrs = list(dec) 
                del dtrs[dtrs.index(-pidl)]
                del dtrs[dtrs.index(pidl)]
                dtrs += [-pidl, pidl]
                pde.addChannel(1, br, me, *dtrs)
    
            # Run the particle gun.
            if pgun.sigma*br < sigmamin: continue
            print(("%s ->" + " %s"*len(dec)) % tuple(
                [pdb.name(pid)] + [pdb.name(dtr) for dtr in dec]))
            bar = cimba.utils.Progress(ntry)
            for itry in range(0, int(ntry)):
                bar.next()
                px, py, pz, wgt = pgun()
                event = decay(pid, px, py, pz)
                plps, plms = [], []
                for prt in event:
                    if prt.id() == pidl: plps += [prt.p()]
                    elif prt.id() == -pidl: plms += [prt.p()]
                if len(plps) == 0 or len(plms) == 0: continue
    
                # Fill the mass histogram.
                for plp, plm in zip(plps, plms):
                    if not fiducial(plp, plm): continue
                    m = (plp + plm).mCalc()
                    hsts[(pid, dec)][hst[m]] += wgt*br*pgun.sigma*(
                        (mres/bwdh)*(lumi*1e12/ntry))
    
        # Turn mother decay off.
        pdb.mayDecay(pid, False)
    
    # Sum the histograms and return.
    tot = [0.]*len(hst.xs)
    for (mom, dec), ys in hsts.items():
        for i in range(0, len(ys) - 1): tot[i] += ys[i]
    hsts["total"] = tot
    del decay.pythia, decay
    return hsts

###############################################################################
def plot(name, hsts, ylim = (None, None), yscale = "log", nchn = 3):
    """
    Print the histogram results to screen and plot the result.

    name:   name of the output plot.
    hsts:   histograms to plot.
    ylim:   y-limits for the plot.
    yscale: scale type for the y-axis.
    nchn:   number of top channels to include in the plot.
    """
    # Sort the results.
    chns = [(sum(val[:-2]), val, key) for key, val in hsts.items()
            if key != "xs"]
    chns.sort(reverse = True)
    
    # Print and plot the results.
    pythia = pythia8.Pythia("", False)
    pdb, keys = pythia.particleData, []
    for n, ys, key in chns:
        if n == 0: break
        if type(key) == tuple:
            mom, dec = key
            key = ("%s ->" + " %s"*len(dec)) % tuple(
                [pdb.name(mom)] + [pdb.name(dtr) for dtr in dec])
        keys += [key]
        print(("%9.2e: %s") % (n, key))

    # Plot the results, with the top three channels included.
    pyplot = cimba.utils.pyplot()
    if not pyplot: return
    fig, ax = pyplot.subplots()
    xs = hsts["xs"]
    for key, (n, ys, dec) in zip(keys[0:nchn + 1], chns[0:nchn + 1]):
        key = key if key == "total" else "$" + " ".join([
            cimba.utils.latex(word) for word in key.split()]) + "$"
        ax.plot(*cimba.utils.plot((xs, ys)), label = key)
        ax.set_xlabel(r"$m_{\ell\ell}$ [GeV]")
        ax.set_ylabel(r"events per $4\sigma$ mass bin")
        ax.set_xlim([xs[0], xs[-1]])
        if ylim[0] or ylim[1]: ax.set_ylim(ylim)
        if yscale: ax.set_yscale(yscale)
        ax.legend()
        cimba.utils.logo()
        fig.savefig(name)
    del pythia
    
###############################################################################
if __name__ == "__main__":
    """
    Generate di-muon and di-electron spectrums.
    """

    # Generate the di-muon EM spectrum following the requirements of
    # arXiv:1603.08926.
    hsts = generate(pidl = 13, ntry = 1e4, bwdh = 0.01, bmin = 0.2, bmax = 1.0,
                    ptlim = (1.0, None), etalim = (2., 5.), lumi = 15,
                    mres = 0.016)
    
    # Print the result to screen and plot.
    plot("dilepton_mu.pdf", hsts, ylim = (1e7, 1e10))

    # Do the same but for di-electrons.
    hsts = generate(pidl = 11, ntry = 1e4, bwdh = 0.01, bmin = 0.0, bmax = 1.0,
                    ptlim = (1.0, None), etalim = (2., 5.), lumi = 15,
                    mres = 0.02*4)
    
    # Print the result to screen and plot.
    plot("dilepton_e.pdf", hsts, ylim = (1e8, 1e13))
