# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.

# Demonstrates how to sample a 1D PDF with cubic interpolation.

# Update the system path to find the CIMBA module.
# This assumes that 'examples' is in 'cimba/examples.'
import sys, os, inspect, itertools
sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(
                inspect.getfile(inspect.currentframe()))), "../../"))

# Load the CIMBA module.
import cimba

# Setup the random number generation.
import random
rng = random.Random()
rng.seed(1)

# Create the generating distribution (note this is not normalized).
xbin = [-5, -2, -1, 0, 3, 4, 5]
xgen = [(xbin[i] + xbin[i + 1])/2. for i in range(0, len(xbin) - 1)]
ygen = [5, 3, 6, 8, 1, 0]

# The sampled values must be floats.
xgen = [float(x) for x in xgen]
ygen = [float(y) for y in ygen]

# Create the sampler.
pdf = cimba.PDF(xgen, ygen)
sample = cimba.Sample1D(pdf)

# Create the histogram to fill.
step = 0.2
xhst = cimba.Histogram()
xhst.xs = [xgen[0] + i*step for i in
           range(0, int((xgen[-1] - xgen[0])/step + 1) + 1)]
yall = [0]*(len(xhst.xs) - 1)

# Sample events and fill the histogram.
ntry = 1e5
for i in range(0, int(ntry)):
    x, w = sample(rng.random())
    yall[xhst[x]] += sample.norm*w/(ntry*step)

# Sample again, but now only over the range 0 < x < 2.
sample = cimba.Sample1D(pdf, xlim = (0, 2))
ylim = [0]*(len(xhst.xs) - 1)
for i in range(0, int(ntry)):
    x, w = sample(rng.random())
    ylim[xhst[x]] += sample.norm*w/(ntry*step)
    
# Plot the distributions.
pyplot = cimba.utils.pyplot()
if pyplot:
    pyplot.plot(*cimba.utils.plot((xbin, ygen)), label = "PDF")
    pyplot.plot(*cimba.utils.plot((xhst.xs, yall)), label = "CIMBA")
    pyplot.plot(*cimba.utils.plot((xhst.xs, ylim)), label = "limited")
    pyplot.legend()
    cimba.utils.logo()
    pyplot.savefig("sample1d.pdf")
