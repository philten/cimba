# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.

# Demonstrates how to sample a 2D PDF with cubic interpolation.

# Update the system path to find the CIMBA module.
# This assumes that 'examples' is in 'cimba/examples.'
import sys, os, inspect, itertools
sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(
                inspect.getfile(inspect.currentframe()))), "../../"))

# Load the CIMBA module.
import cimba

# Setup the random number generation.
import random
rng = random.Random()
rng.seed(1)

# Create the generating distribution (note this is not normalized).
xbin = [-5, -1, 0, 3, 4, 5]
ybin = [0, 1, 2, 3]
xgen = [(xbin[i] + xbin[i + 1])/2. for i in range(0, len(xbin) - 1)]
ygen = [(ybin[i] + ybin[i + 1])/2. for i in range(0, len(ybin) - 1)]
zgen = [[2, 3, 4, 1, 2],
        [0, 1, 0, 0, 0],
        [3, 2, 2, 8, 1]]

# The sampled values must be floats.
xgen = [float(x) for x in xgen]
ygen = [float(y) for y in ygen]
zgen = [[float(z) for z in zrow] for zrow in zgen]

# Create the sampler (a list of floats must be passed).
pdfs = [cimba.PDF(xgen, zrow) for zrow in zgen]
sample = cimba.Sample2D(xgen, ygen, pdfs)

# Create the histogram to fill.
step = 0.2
xhst = cimba.Histogram()
xhst.xs = [xgen[0] + i*step for i in
           range(0, int((xgen[-1] - xgen[0])/step + 1) + 1)]
yhst = cimba.Histogram()
yhst.xs = [ygen[0] + i*step for i in
           range(0, int((ygen[-1] - ygen[0])/step + 1) + 1)]
zall = [[0]*(len(xhst.xs) - 1) for i in range(0, (len(yhst.xs) - 1))]

# Sample events and fill the histogram.
ntry = 1e5
for i in range(0, int(ntry)):
    x, y, w = sample(rng.random(), rng.random())
    zall[yhst[y]][xhst[x]] += sample.norm*w/(ntry*step*step)
    
# Sample again, but now only over the range 2 < x < 4 and 1 < y < 2.5.
sample =cimba.Sample2D(xgen, ygen, pdfs, xlim = (2, 4), ylim = (1, 2.5))
zlim = [[0]*(len(xhst.xs) - 1) for i in range(0, (len(yhst.xs) - 1))]
for i in range(0, int(ntry)):
    x, y, w = sample(rng.random(), rng.random())
    zlim[yhst[y]][xhst[x]] += sample.norm*w/(ntry*step*step)

# Plot the distributions.
pyplot = cimba.utils.pyplot()
if pyplot:
    fig, ax = pyplot.subplots(2, 2)
    ax[0, 0].pcolor(xbin, ybin, zgen, vmin = 0, vmax = 8)
    ax[1, 0].pcolor(xhst.xs, yhst.xs, zall, vmin = 0, vmax = 8)
    ax[1, 0].set_xlabel("CIMBA")
    ax[1, 1].pcolor(xhst.xs, yhst.xs, zlim, vmin = 0, vmax = 8)
    ax[1, 1].set_xlabel("limited")
    for i in range(0, 2):
        for j in range(0, 2):
            ax[i, j].set_xlim([xgen[0], xgen[-1]])
            ax[i, j].set_ylim([ygen[0], ygen[-1]])
    cimba.utils.logo()
    pyplot.savefig("sample2d.pdf")
