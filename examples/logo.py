# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.

# This example draws the CIMBA logo, in case anyone needs it!

# Update the system path to find the CIMBA module.
# This assumes that 'examples' is in 'cimba/examples.'
import sys, os, inspect, itertools
sys.path.insert(1, os.path.join(os.path.dirname(os.path.realpath(
                inspect.getfile(inspect.currentframe()))), "../../"))

# Load the CIMBA module.
import cimba

# Draw the logo.
pyplot = cimba.utils.pyplot()
if pyplot:
    cimba.utils.logo(0, 0, 1)
    pyplot.savefig("logo.pdf", bbox_inches = "tight")
