# CIMBA

CIMBA, Cubic Interpolation for Minimum Bias approximation, is the companion software package to the paper [*CIMBA: fast Monte Carlo generation using cubic interpolation*](https://arxiv.org/abs/) and allows users to quickly generate and decay single particles produced from minimum bias events in particle collisions. The CIMBA package is written as a module in Python and the interpolation and generation routines have no external dependencies. To decay particles, the Python modules for [Pythia8](http://home.thep.lu.se/~torbjorn/Pythia.html) must be available. Pythia 8 must be version 8.219 or higher, and have been configured with the `--with-python` command. Details can be found in the [Pythia 8 HTML manual](https://pythia8.gitlab.io/docs/manual/8243/PythonInterface.html).

To begin using CIMBA, download the source and try running some of the examples:
```bash
wget https://gitlab.com/philten/cimba/-/archive/master/cimba-master.tar.gz
tar -xzvf cimba-master.tar.gz
mv cimba-master cimba
cd cimba/examples
```

There are a number of examples provided and each can be run as:
```bash
python <example>.py
```
where `<example>.py` is the relevant example. If the Python plotting module `matplotlib` is available, all the examples will produce plots in the PDF format.

1. `sample1d.py`: samples a 1D PDF with cubic interpolation and produces `sample1.pdf` plots the results for the generating PDF histogram, CIMBA, and CIMBA with limits applied.
2. `sample2d.py`: the same as `sample1d.py` but now for a 2D PDF with output `sample2d.pdf`.
3. `pgun.py`: generates a single particle, specified by the user. This example can be run either as a script where particle gun generation is performed for $`\pi^+`$ or be imported as a module, where the `generate` method can be customized by the user to generate any particle of choice. Optionally, if the `pythia8` module is available, the distribution from CIMBA is compared to the distribution from Pythia. Four distributions are plotted: `pgun_grid.pdf` is the sampling grid in $`\rho`$ and $`\eta`$, `pdf_rho.pdf` is the 1D profile of $`\rho`$, *e.g.* with $`\eta`$ integrated out, `pgun_rho.pdf` is the pseudorapidity distribution, and `pgun_pt.pdf` is the transverse momentum distribution. Further documentation is available via `help(generate)`.
4. `dilepton.py`: generates the inclusive di-lepton spectrum, where the lepton flavor can be specified by the user. This example can be run as a script or imported as a module, where the `generate` method provides the main routine. The plots produced are `dilepton_mu.pdf` which corresponds to the inclusive di-muon dark photon search of [arXiv:1603.08926](https://arxiv.org/abs/1603.08926) and `dilepton_e.pdf` which is based on the true muonium search of [arXiv:1904.08458](https://arxiv.org/abs/1904.08458).
5. `logo.py`: draws the CIMBA logo.

The following is a simple usage example which generates $`\pi^+`$ mesons with the 14 TeV LHC grid.
```python
# Load the module.
import cimba

# Create the random number generator.
import random
rng = random.Random()

# Load the grid.
grid = cimba.grid('data/pp14TeV.pkl')

# Create the particle gun.
pgun = cimba.ParticleGun(grid, "all/211", rng.random)

# Generate a pi+.
px, py, pz, weight = pgun()
```

## References

When using CIMBA, please cite [*CIMBA: fast Monte Carlo generation using cubic interpolation*](https://arxiv.org/abs/). If the provided generation grids are used, please also cite [*An Introduction to PYTHIA 8.2*](https://arxiv.org/abs/1410.3012).

## Licensing

CIMBA is licensed under the GNU GPL version 2, or later and is copyrighted (C) 2020 by Philip Ilten.

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU General Public License](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html) or [LICENSE](LICENSE) for more details.

## Structure

The structure of CIMBA is as follows, beginning at the top level.

* `README.md`: is this file.
* `__init__.py`: initializes the CIMBA package.
* `interpolate.py`: defines the classes needed for sampling a PDF defined with cubic interpolation. These include `PDF`, `CDF`, `InverseCDF`, `Sample1D`, and `Sample2D`. Additionally the classes `SampleTH1`, `SampleTH2`, and `SampleTH2s` are provided to directly sample a ROOT histogram.
* `generate`: defines the `ParticleGun` class used to generate particles and the `grid` method to load interpolation grids. Additionally, `PythiaDecay` provides particle decays, but requires the `pythia8` module.
* `utils.py`: contains auxiliary utilities which are not physics related.

### [examples](examples)

The examples enumerated above are provided here.

### [data](data)

Grids available for interpolation are stored here. These include the following:

* `pp7TeV.pkl`: soft QCD at the LHC and a COM of 7 TeV.
* `pp8TeV.pkl`: soft QCD at the LHC and a COM of 8 TeV.
* `pp13TeV.pkl`: soft QCD at the LHC and a COM of 13 TeV.
* `pp14TeV.pkl`: soft QCD at the LHC and a COM of 14 TeV.
* `pp27GeV.pkl`: soft QCD at SHiP with a proton beam energy of 400 GeV.
* `ppbar980GeV.pkl`: soft QCD at the Tevatron and a COM of 980 GeV.

Each set of grids has been generated with roughly 1 billion events from Pythia 8. Further grids can be generated upon request.

### [grids](grids)

Code is provided here to generate custom interpolation grids and convert them into the CIMBA format using Pythia 8 and ROOT. Further details are given in the [`README.md`](grids/README.md) of the `grids` directory.

## History

* **v0.1** - 01/08/2019
  * First beta release of the full CIMBA code.
