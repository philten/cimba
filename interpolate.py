# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.
from warnings import warn
from math import sin, cos, acos, pi
from .utils import sqrt, cbrt

###############################################################################
class Histogram(object):
    """
    This is an abstract class for 1D histograms, based on the Histogram
    class from the DARKCAST package.

    xs: a list of the bin edges.
    """

    ###########################################################################
    def __len__(self):
        """
        Return the number of bins.
        """
        return len(self.xs) - 1

    ###########################################################################
    def __getitem__(self, x):
        """
        Find the bin index for a given x using modified regula falsi.

        x: x-value for which to determine the bin index.
        """
        xs = self.xs
        k, k0, k1 = 0, 0, len(xs) - 1
        if x <= xs[k0]:   k1 = k0
        elif x >= xs[k1]: k0 = k1
        else:
            while k1 - k0 > 1:
                k = k0 + int(round((x - xs[k0])*(k1 - k0)/(xs[k1] - xs[k0])))
                if x == xs[k]: k0, k1 = k, k; break
                if k == k0 or k == k1: k = k0 + (k1 - k0)//2
                if x > xs[k]: k0 = k
                else: k1 = k
        return k0

###############################################################################
class PDFError(Exception):
    """
    Simple exception for the 'PDF' class.
    """
    pass

###############################################################################
class PDF(Histogram):
    """
    Given data points, interpolate using a piecewise cubic hermite
    interpolating polynomial (PCHIP). The polynomial takes the form:

    f(x) = c0 + c1*x + c2*x**2 + c3*x**3 + c4*x**4

    xs:  x-values defining the interpolation.
    ys:  y-values defining the interpolation.
    cis: interpolating coefficients for the term of order i, 
         where 0 <= i <= 4.
    """
    ###########################################################################
    def __init__(self, xs, ys, pdfs = None):
        """
        Initialize the interpolating coefficients given a set of data
        points. For cubic interpolation there must be at least three
        data points.

        Optionally, a list of PDFs used for 2D interpolation can be
        passed, and the PDF integrated over y is returned. Each PDF in
        the list corresponds to a y-value provided in 'ys', while each
        PDF itself has x-values given by 'xs'. The grid spacing of the
        PDFs in y must be constant.

        xs:   x-values defining the interpolation.
        ys:   y-values defining the interpolation.
        pdfs: optional, PDFs defining a 2D interpolation, one for each 
              y-value of 'ys' with data points defined at 'xs'.
        """
        # Simple error checks.
        if len(xs) < 3: raise PDFError(
                "The 'xs' length of %i is less than 3." % len(xs))
        if len(ys) < 3: raise PDFError(
                "The 'ys' length of %i is less than 3." % len(ys))
        
        # 1D interpolation.
        if not pdfs:
            if len(xs) != len(ys): raise PDFError(
                    "The 'xs' length of %i does not match the 'ys' "
                    "length of %i." % (len(xs), len(ys)))
            dxs = [xs[i + 1] - xs[i] for i in range(0, len(xs) - 1)]
            ms  = [(ys[i + 1] - ys[i])/dx for i, dx in enumerate(dxs)]
            
            # First degree coefficients.
            c1s = [ms[0]]
            for i in range(0, len(dxs)  - 1):
                m0, m1, dx0, dx1 = ms[i], ms[i + 1], dxs[i], dxs[i + 1]
                c = dx0 + dx1
                c1s += [0 if m0*m1 <= 0 else 3*c/((c + dx1)/m0 + (c + dx0)/m1)]
            c1s += [ms[-1]]
                
            # Second and third degree coefficients.
            c2s, c3s = [], []
            for i in range(0, len(c1s) - 1):
                c10, c11, m0, dx0 = c1s[i], c1s[i + 1], ms[i], dxs[i]
                c = c10 + c11 - 2*m0
                c2s += [(m0 - c10 - c)/dx0]
                c3s += [c/dx0**2]

        # 2D interpolation integrated over y.
        else:
            dy = ys[1] - ys[0]
            if len(pdfs) != len(ys):
                raise PDFError(
                    "The 'pdfs' length of %i must be %i." %
                    (len(pdfs), len(ys)))
            if not all([(ys[j] - ys[j - 1] - dy)/dy < 1e-2
                        for j in range(1, len(ys))]):
                raise PDFError(
                    "The y-axis grid spacing is not constant.")
            m, cs = len(pdfs), [[0.]*(len(xs) - 1) for ci in range(0, 4)]

            # Create the coefficients.
            for i in range(0, len(xs) - 1):
                for j, pdf in enumerate(pdfs):
                    if j == 0 or j == m - 1: f = 5./12.
                    elif m == 3: f = 14./12.
                    elif j == 1 or j == m - 2: f = 13./12.
                    else: f = 1.
                    for k, cys in enumerate([
                            pdf.c0s, pdf.c1s, pdf.c2s, pdf.c3s]):
                        cs[k][i] += f*dy*cys[i]
            ys, c1s, c2s, c3s = cs[0], cs[1], cs[2], cs[3]
            
        # Save the needed values.
        self.xs, self.ys = [x for x in xs], [y for y in ys]
        self.c0s, self.c1s, self.c2s, self.c3s = self.ys, c1s, c2s, c3s

    ###########################################################################
    def __call__(self, x):
        """
        Return the interpolated polynomial.

        x: x-value at which to evaluate the interpolating polynomial.
        """
        i = min(self[x], len(self) - 1)
        dx = x - self.xs[i]
        return (self.c0s[i] + self.c1s[i]*dx + self.c2s[i]*dx**2 +
                self.c3s[i]*dx**3)
    
    ###########################################################################
    def integral(self, i, x = None):
        """
        Return the integral for a given x-value and interval, or just
        a given interval.

        i: interval for which to evaluate the integral.
        x: optional, the x-value defining the sub-interval within 'i'.
        """
        if x == None: x = self.xs[i + 1]
        dx = x - self.xs[i]
        return (self.c0s[i]*dx + self.c1s[i]*dx**2/2. + self.c2s[i]*dx**3/3.
                + self.c3s[i]*dx**4/4.)

###############################################################################
class CDF(Histogram):
    """
    Provide the cumulative distribution function for a PDF defined
    with cubic interpolation. This is a piecewise function
    consisting of fourth-degree polynomials (or lower).
    
    xs:  x-values defining the interpolation.
    ys:  y-values defining the integrated interpolation.
    pdf: PDF defining the CDF.
    """
    ###########################################################################
    def __init__(self, pdf):
        """
        Initialize the CDF from the PDF.

        pdf: PDF, defined with cubic interpolation, for which to create
             the CDF.
        """
        self.pdf = pdf
        self.xs = [x for x in self.pdf.xs]
        self.ys = [0.]
        for i in range(0, len(self.pdf)):
            self.ys += [self.ys[i] + self.pdf.integral(i)]
        
    ###########################################################################
    def __call__(self, x):
        """
        Return the CDF for a given x.

        x: x-value at which to evaluate the CDF.
        """
        if self.ys[-1] <= 0: return 0.
        i = min(self[x], len(self) - 1)
        return (self.ys[i] + self.pdf.integral(i, x))/self.ys[-1]

###############################################################################
class InverseCDFWarning(Warning):
    """
    Simple warning for the 'InverseCDF' class.
    """
    pass
    
###############################################################################
class InverseCDF(CDF):
    """
    Provide the inverse cumulative distribution function of a PDF
    defined with cubic interpolation. The inverse CDF is a piecewise
    function consisting of four-degree polynomials (or lower).

    xs:   CDF y-values defining the integrated interpolation.
    ys:   CDF x-values defining the interpolation.
    xmin: minimum CDF y-value (must be within 0 <= xmin <= 1).
    xmax: maximum CDF y-value (must be within 'xmin' < xmax <= 1).
    xdif: difference between 'xmax' and 'xmin'
    """
    
    ###########################################################################
    def __init__(self, pdf, xlim = (None, None)):
        """
        Initialize the inverse CDF from a PDF. Optionally, provide
        x-limits for the PDF over which to define the inverse CDF,
        e.g. a range of the PDF for which to calculate the inverse
        CDF.

        pdf:  PDF, defined with cubic interpolation, for which to create
              the inverse CDF.
        xlim: tuple of (xmin, xmax) defining the PDF x-range.
        """
        super(InverseCDF, self).__init__(pdf)
        self.xmin, self.xmax = 0., 1.
        if xlim[0] != None and xlim[0] > self.xs[0]:
            self.xmin = super(InverseCDF, self).__call__(xlim[0])
        if xlim[1] != None and xlim[1] < self.xs[-1]:
            self.xmax = super(InverseCDF, self).__call__(xlim[1])
        self.xs, self.ys = self.ys, self.xs
        self.xdif = self.xmax - self.xmin

    ###########################################################################
    def _check(self, cn):
        """
        Check numerical conditioning on a polynomial.

        cn: highest order coefficient of the polynomial.
        """
        return abs(cn) < 1e-20
    
    ###########################################################################
    def _roots(self, x, i, c0, c1, c2, c3, c4):
        """
        Fall-back method when analytic solution of the quartic
        equation fails for numerical reasons. First, try to use the
        companion matrix method provided in the 'numpy' package, next,
        just perform linear interpolation.

        x:  x-value being solved for.
        i:  interval for evaluation.
        ci: polynomial coefficents where 0 <= i <= 4.
        """
        try:
            import numpy
            return min([y for y in numpy.roots([c4, c3, c2, c1, c0])
                        if numpy.isreal(y) and y > 0]).real
        except: return (self.ys[i + 1] - self.ys[i])/(
                self.xs[i + 1] - self.xs[i])*(x - self.xs[i])

    ###########################################################################
    def _solve1(self, c0, c1):
        """
        Find the zero for a first-degree interpolating polynomial.

        f(x) = c0 + c1*x = 0

        ci: polynomial coefficents where 0 <= i <= 1.
        """
        return -c0/c1

    ###########################################################################
    def _solve2(self, c0, c1, c2):
        """
        Find the zero for a second-degree interpolating polynomial.

        f(x) = c0 + c1*x + c2*x**2 = 0
        
        ci: polynomial coefficents where 0 <= i <= 2.
        """
        if self._check(c2): return self._solve1(c0, c1)
        c1 /= c2; c0 /= c2
        q = -(c1 + (1. if c1 > 0 else -1.)*sqrt(c1**2. - 4.*c0))/2.
        return q if q >= 0 and q**2. < c0 else c0/q

    ###########################################################################
    def _solve3(self, c0, c1, c2, c3):
        """
        Find the zero for a third-degree interpolating polynomial.

        f(x) = 0 = c0 + c1*x + c2*x**2 + c3*x**3
        
        ci: polynomial coefficents where 0 <= i <= 3.
        """
        if self._check(c3): return self._solve2(c0, c1, c2)
        c2 /= c3; c1 /= c3; c0 /= c3
        p = (2.*c2**3 - 9.*c2*c1 + 27.*c0)/54.
        q = (c2**2 - 3.*c1)/9.

        # Single real root.
        if p**2 > q**3:
            s = (-1. if p > 0 else 1.)*cbrt(abs(p) + sqrt(p**2. - q**3.))
            return s + q/s - c2/3.
        # Three real roots.
        else:
            return -2.*sqrt(q)*cos((acos(p/sqrt(q**3)) - 2*pi)/3.) - c2/3.

    ###########################################################################
    def _solve4(self, c0, c1, c2, c3, c4):
        """
        Find the zero for a fourth-degree interpolating polynomial.

        f(x) = 0 = c0 + c1*x + c2*x**2 + c3*x**3 + c4*x**4
        
        ci: polynomial coefficents where 0 <= i <= 4.
        """
        if self._check(c4): return self._solve3(c0, c1, c2, c3)
        c3 /= c4; c2 /= c4; c1 /= c4; c0 /= c4
        p  = (8.*c2 - 3.*c3**2)/4.
        q  = (c3**3 - 4.*c3*c2 + 8.*c1)/8.
        d0 = c2**2 - 3.*c3*c1 + 12.*c0
        d1 = 2.*c2**3 - 9.*c3*c2*c1 + 27.*c3**2*c0 + 27.*c1**2 - 72.*c2*c0
        # Two real roots.
        if d1**2 > 4.*d0**3:
            r = cbrt(d1 if d0 == 0 else 0.5*(d1 + sqrt(d1**2 - 4.*d0**3)))
            s = sqrt((r + d0/r - p)/3.)/2. if r != 0 else float("NaN")
            t = -1. if q/s - p - 4*s**2 < 0 else 1.
        # Four real roots.
        else:
            s = sqrt((sqrt(d0)*cos(acos(d1/(2.*d0**(3./2.)))/3.) - p/2.)/6.)
            t = -1. if c3/4. + s > 0.5*sqrt(q/s - p - 4.*s**2) else 1.
        return -t*(s - 0.5*sqrt(t*q/s - p - 4*s**2)) - c3/4.
            
    ###########################################################################
    def __call__(self, x):
        """
        Return the inverse CDF for a given x.

        x: x-value for which to evaluate the inverse CDF.
        """
        if self.xs[-1] <= 0: return 0., self.xdif
        x = (self.xmin + self.xdif*x)*self.xs[-1]
        i = min(self[x], len(self))
        if x == self.xs[i]: return self.ys[i], self.xdif
        y = self._solve4(self.xs[i] - x, self.pdf.c0s[i], self.pdf.c1s[i]/2.,
                         self.pdf.c2s[i]/3., self.pdf.c3s[i]/4.) + self.ys[i]
        if not self.ys[i] < y < self.ys[i + 1]:
            y = self._roots(
                x, i, self.xs[i] - x, self.pdf.c0s[i], self.pdf.c1s[i]/2.,
                self.pdf.c2s[i]/3., self.pdf.c3s[i]/4.) + self.ys[i]
        if not self.ys[i] <= y <= self.ys[i + 1]:
            warn("The returned CDF value of %.2e does not fall within"
                 " %.2e < y < %.2e." % (y, self.ys[i], self.ys[i + 1]),
                 InverseCDFWarning)
        return y, self.xdif

###############################################################################
class Sample1D(object):
    """
    Randomly sample values from a 1D PDF defined using cubic
    interpolation.

    pdf:  generating PDF for reference (not used internally).
    icdf: inverse CDF used to sample the distribution.
    norm: normalization for the sampled distribution.
    """
    ###########################################################################
    def __init__(self, pdf, xlim = (None, None)):
        """
        Initialize the 1D PDF sampler. Optionally, provide
        x-limits to restrict the sampling range of the PDF.

        pdf:  PDF, defined with cubic interpolation, for sampling.
        xlim: tuple of (xmin, xmax) defining the PDF x-range.
        """
        self.pdf = pdf
        self.icdf = InverseCDF(self.pdf, xlim)
        self.norm = self.icdf.xs[-1]
        
    ###########################################################################
    def __call__(self, ux):
        """
        Given a uniform random number, 0 <= 'ux' <= 1, return a value
        sampled from the PDF and its weight. This weight is constant,
        and is unity if no x-limits are specified.

        ux: uniform random number, 0 <= 'ux' <= 1.
        """
        return self.icdf(ux)

###############################################################################
class Sample2D(object):
    """
    Randomly sample values from a 2D PDF defined using bi-cubic
    interpolation. The PDF must have constant spacing in y, but may
    have variable spacing in x. The sampling is first performed in x,
    then in y. Consequently, sampling is faster when the x-axis is
    assigned to the smaller PDF dimension (if possible).

    ys:   y-values defining the interpolation grid.
    ylim: tuple of (ymin, ymax) defining the PDF y-limits.
    pdf:  generating PDF, integrated over y for reference (not used internally).
    pdfs: interpolating PDF in x for a given y-value.
    icdf: inverse CDF for the interpolating PDF integrated over y.
    swap: if 'True', swap the sampled x and y values.
    """
    ###########################################################################
    def __init__(self, xs, ys, pdfs, xlim = (None, None), ylim = (None, None),
                 swap = False):
        """
        Initialize the 2D PDF sampler. Optionally, provide
        x-limits and y-limits to restrict the sampling range of the PDF.

        xs:   x-values defining the interpolation.
        ys:   y-values defining the interpolation.
        pdfs: PDFs defining the interpolation in x for each y-value.
        xlim: tuple of (xmin, xmax) defining the PDF x-range.
        ylim: tuple of (ymin, ymax) defining the PDF y-range.
        swap: if 'True', swap the sampled x and y values.
        """
        self.ys, self.ylim, self.pdfs = ys, ylim, pdfs
        self.pdf = PDF(xs, ys, pdfs)
        self.icdf = InverseCDF(self.pdf, xlim)
        self.norm = self.icdf.xs[-1]
        self.swap = False

    ###########################################################################
    def __call__(self, ux, uy):
        """
        Given two uniform random numbers, 0 <= 'ux' <= 1 and 0 <= 'ux'
        <= 1, return a value sampled from the PDF and its weight. This
        weight is guaranteed to be constant when no y-limits are
        specified, and is unity if no x or y-limits are specified.
        
        ux: uniform random number, 0 <= 'ux' <= 1.
        uy: uniform random number, 0 <= 'uy' <= 1.
        """
        x, wx = self.icdf(ux)
        y, wy = InverseCDF(PDF(self.ys, [pdf(x) for pdf in self.pdfs]),
                               self.ylim)(uy)
        return (y, x, wx*wy) if self.swap else (x, y, wx*wy)
    
###############################################################################
class SampleTH1(Sample1D):
    """
    Sample values from a 1D PDF defined using cubic interpolation of a
    ROOT TH1 distribution. The histogram entries must already be
    scaled by the bin widths. This method inherits from 'Sample1D' and
    has no additional members.
    """
    ###########################################################################
    def __init__(self, hst, xlim = (None, None), xbin = "center"):
        """
        Initialize the sampler for a ROOT TH1. The histogram entries must
        already be scaled by the bin widths.

        hst:  TH1 defining the PDF.
        xlim: tuple of (xmin, xmax) defining the PDF x-range.
        xbin: keyword of what x-values should be used to define the PDF of
              'lower', 'center', 'upper'.
        """
        ax = hst.GetXaxis()
        bx = ax.GetBinCenter
        if "lower" in xbin.lower(): bx = ax.GetBinLowEdge
        if "upper" in xbin.lower(): bx = ax.GetBinUpEdge
        nx = ax.GetNbins() + 1
        xs = [bx(i) for i in range(1, nx)]
        ys = [hst.GetBinContent(i) for i in range(1, nx)]
        super(SampleTH1, self).__init__(PDF(xs, ys), xlim)

###############################################################################
class SampleTH2(Sample2D):
    """
    Sample values from a 2D PDF defined using bi-cubic interpolation of a
    ROOT TH2 distribution. The histogram entries must already be
    scaled by the bin widths. This method inherits from 'Sample2D' and
    has no additional members.
    """
    ###########################################################################
    def __init__(self, hst, xlim = (None, None), ylim = (None, None),
                 swap = True, xbin = "center", ybin = "center"):
        """
        Initialize the sampler for a ROOT TH2. The histogram entries must
        already be scaled by the bin widths.

        hst:  TH2 defining the PDF.
        xlim: tuple of (xmin, xmax) defining the PDF x-range.
        ylim: tuple of (ymin, ymax) defining the PDF y-range.
        swap: if 'True', swap the PDF axes when needed. The sampled output
              order will still correspond to the original axes.
        xbin: keyword of what x-values should be used to define the PDF of
              'lower', 'center', 'upper'.
        ybin: keyword of what x-values should be used to define the PDF of
              'lower', 'center', 'upper'.
        """
        ax, ay = hst.GetXaxis(), hst.GetYaxis()
        bx, by = ax.GetBinCenter, ay.GetBinCenter
        if "lower" in xbin.lower(): bx = ax.GetBinLowEdge
        if "upper" in xbin.lower(): bx = ax.GetBinUpEdge
        if "lower" in ybin.lower(): by = ay.GetBinLowEdge
        if "upper" in ybin.lower(): by = ay.GetBinUpEdge
        nx, ny = ax.GetNbins() + 1, ay.GetNbins() + 1
        xs, ys = [bx(i) for i in range(1, nx)], [by(i) for i in range(1, ny)]
        dy = ys[1] - ys[0]
        
        # Swap axes if needed and requested, and initialize.
        cnst = all([ys[j] - ys[j - 1] == dy for j in range(1, ny - 1)])
        swap = swap and (len(ys) > len(xs) or not cnst)
        if swap:
            xlim, ylim, ax, ay = ylim, xlim, ay, ax
            nx, ny, xs, ys = ny, nx, ys, xs
        super(SampleTH2, self).__init__(xs, ys, [PDF(xs, [
            hst.GetBinContent(*((j, i) if swap else (i, j))) for
            i in range (1, nx)]) for j in range(1, ny)], xlim, ylim, swap)

###############################################################################
class SampleTH2s(Histogram):
    """
    Given an x-value, sample y and z-values from a 2D PDF defined
    using bi-cubic interpolation. The 2D PDFs are defined for x-value
    ranges from a TH3. This is not tri-cubic sampling. The histogram
    entries must already be scaled by the bin widths.
    """
    ###########################################################################
    def __init__(self, hst, ylim = (None, None), zlim = (None, None),
                 swap = True, ybin = "center", zbin = "center"):
        """
        Initialize the sampler for a ROOT TH3. The histogram entries must
        already be scaled by the bin widths.

        hst:  TH3 defining the 2D PDFs.
        ylim: tuple of (ymin, ymax) defining the PDF y-range.
        zlim: tuple of (zmin, zmax) defining the PDF z-range.
        swap: if 'True', swap the PDF axes when needed. The sampled output
              order will still correspond to the original axes.
        ybin: keyword of what y-values should be used to define the PDF of
              'lower', 'center', 'upper'.
        zbin: keyword of what z-values should be used to define the PDF of
              'lower', 'center', 'upper'.
        """
        import ROOT, array
        ax, ay, az = hst.GetXaxis(), hst.GetYaxis(), hst.GetZaxis()
        self.xs, self.samplers = [], []

        # Loop over the slices.
        for i in range(1, ax.GetNbins() + 1):
            # Grab the coordinates.
            x = ax.GetBinLowEdge(i)
            ys = [ay.GetBinLowEdge(j) for j in range(1, ay.GetNbins() + 2)]
            zs = [az.GetBinLowEdge(k) for k in range(1, az.GetNbins() + 2)]

            # Create a temporary 2D histogram.
            slc = ROOT.TH2D(str(x), "", len(ys) - 1, array.array("d", ys),
                            len(zs) - 1, array.array("d", zs))
            for j in range(1, ay.GetNbins() + 1):
                for k in range(1, az.GetNbins() + 1):
                    slc.SetBinContent(j, k, hst.GetBinContent(i, j, k))

            # Build the interpolator.
            self.xs += [x]
            self.samplers += [SampleTH2(slc, ylim, zlim, swap, ybin, zbin)]
        self.xs += [ax.GetBinUpEdge(i)]
        
    ###########################################################################
    def __call__(self, x, uy, uz):
        """
        Given an 'x' and two uniform random numbers, 0 <= 'uy' <= 1 and
        0 <= 'uz' <= 1, return a value sampled from the PDF and its
        weight. This weight is guaranteed to be constant when no
        z-limits are specified, and is unity if no y or z-limits are
        specified.
        
        uy: uniform random number, 0 <= 'uy' <= 1.
        uz: uniform random number, 0 <= 'uz' <= 1.
        """
        return self.samplers[min(self[x], len(self))](uy, uz)
