# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.
import os, inspect, sys, time

###############################################################################
# Create the data paths to search.
paths = []
if os.getenv("CIMBA_DATA_PATH"):
    paths += [os.path.abspath(os.path.expandvars(path)) for path in 
              reversed(os.getenv("CIMBA_DATA_PATH").split(":"))]
paths.append(
    os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe()))))

###############################################################################
def find(name):
    """
    Find a file, either absolute or along the following paths in the
    order given:
    (0) The absolute path, if the absolute path is given.
    (1) The current directory within the Python interpreter.
    (2) The paths defined by the environment variable 'CIMBA_DATA_PATH'.
    (3) The CIMBA package directory.

    Returns the absolute path if it exists, otherwise 'None' is
    returned.

    name: name of the absolute or relative file to find.
    """
    name = os.path.expandvars(name)
    if os.path.isabs(name) and os.path.isfile(name):
        return name
    if os.path.isfile(os.path.join(os.getcwd(), name)):
        return os.path.join(os.getcwd(), name)
    for path in paths:
        if os.path.isfile(os.path.join(path, name)):
            return os.path.join(path, name)
    return None

###############################################################################
def sqrt(x):
    """
    Return the square root.

    x: value to calculate the square root.
    """
    return x**(1./2.) if x >= 0 else float("NaN")

###############################################################################
def cbrt(x):
    """
    Return the cube root.

    y: value to calculate the cube root.
    """
    return x**(1./3.) if x >= 0 else -(-x)**(1./3.)

###############################################################################
def plot(obj, lims = [(-10, 10)], steps = 100):
    """
    Evaluate a function for plotting.

    obj:   this can either be a 1D or 2D ROOT histogram, a 1D function-like 
           object, or a histogram like object, e.g. a tuple of (x-values, 
           y-values).
    lims:  limits to place on the axes (used for function-like objects).
    steps: number of steps to perform for function-like objects.
    """
    # ROOT histogram.
    try:
        n = obj.GetDimension()
        
        # 1D histogram.
        if n == 1:
            ax = obj.GetXaxis()
            xs, ys = [ax.GetBinLowEdge(1)], [0]
            for i in range(1, ax.GetNbins() + 1):
                xs += [ax.GetBinLowEdge(i), ax.GetBinUpEdge(i)]
                ys += [obj.GetBinContent(i), obj.GetBinContent(i)]
            xs += [ax.GetBinUpEdge(i)]; ys += [0]
            return xs, ys

        # 2D histogram.
        elif n == 2:
            ax, ay = obj.GetXaxis(), obj.GetYaxis()
            xs, ys, zs = [ax.GetBinLowEdge(1)], [ay.GetBinLowEdge(1)], []
            for i in range(1, ax.GetNbins() + 1): xs += [ax.GetBinUpEdge(i)]
            for i in range(1, ay.GetNbins() + 1): ys += [ay.GetBinUpEdge(i)]
            for j in range(1, ay.GetNbins() + 1):
                zs += [[]]
                for i in range(1, ax.GetNbins() + 1):
                    zs[-1] += [obj.GetBinContent(i, j)]
            return xs, ys, zs
    except: pass

    # Function-like object.
    try:
        n = len(lims)

        # 1D function.
        if n == 1:
            dx = (lims[0][1] - lims[0][0])/float(steps - 1)
            xs, ys = [], []
            for i in range(0, steps):
                xs += [lims[0][0] + i*dx]
                ys += [obj(lims[0][0] + i*dx)]
            return xs, ys
    except: pass

    # Histogram-like object (1D).
    try:
        xes, ybs = obj
        xs, ys = [xes[0]], [0]
        for i in range(0, len(xes) - 1):
            xs += [xes[i], xes[i + 1]]
            ys += [ybs[i], ybs[i]]
        xs += [xes[i + 1]]; ys += [0]
        return xs, ys
    except: pass
    
###############################################################################
def export(tdir, dct):
    """
    Export a ROOT 'TDirectory' to a dictionary.

    tdir: ROOT 'TDirectory' to export.
    dct:  dictionary to which the 'TDirectory' is exported.
    """
    for key in tdir.GetListOfKeys():
        name, cname = key.GetName(), key.GetClassName() 

        # Recurse sub-directories.
        if cname == "TDirectoryFile":
            dct[name] = {}
            export(key.ReadObj(), dct[name])

        # Handle 'TParameter's. 
        elif cname.startswith("TParameter"):
            dct[name] = key.ReadObj().GetVal()

        # Handle 'TH2's.
        elif cname.startswith("TH2"):
            hst = key.ReadObj()
            nrm = hst.Integral()
            if not nrm: continue
            hst.Scale(1/nrm, "WIDTH")
            xs, ys, zs = plot(hst)
            dct[name] = {"xs": xs, "ys": ys, "zs": zs, "n": nrm}
        else: print(name, cname)
    return dct

###############################################################################
def pyplot():
    """
    Load and configure pyplot from matplotlib.
    """
    try: import matplotlib.pyplot as pyplot
    except: pyplot = None
    try:
        pyplot.rc("text", usetex = True)
        pyplot.rc("text.latex")
        pyplot.rc("font", family = "serif", serif = "cm10", size = 25)
        pyplot.rc("legend", numpoints = 1, handlelength = 1,
                  handletextpad = 0.3, markerscale = 1, loc = "best",
                  frameon = False)
        pyplot.rc("axes", labelpad = 8, labelsize = "larger")
        pyplot.rc("xtick.major", pad = 15)
        pyplot.rc("ytick.major", pad = 15)
        pyplot.rc("lines", linewidth = 3, markersize = 20)
        pyplot.rc("figure", figsize = (10.0, 8.0))
        pyplot.rc("figure.subplot", top = 0.95, bottom = 0.13,
                  left = 0.13, right = 0.95)
    except: pass
    return pyplot
    
###############################################################################
class Progress:
    """
    Simple class to display progress.
    """
    ###########################################################################
    def __init__(self, imax, width = 40):
        """
        Determine the granularity for updating the progress bar.

        imax:  number of iterations.
        width: width of character bar in characters.
        """
        sys.stdout.write("[%s]" % (" " * width))
        sys.stdout.flush()
        sys.stdout.write("\b" * (width + 1))
        self.inow  = 0
        self.imax  = imax
        self.istep = imax/float(width)
        self.inext = self.istep
        self.start = time.time()

    ###########################################################################
    def next(self):
        """
        Iterate to the next event and display progress if needed.
        """
        self.inow += 1
        if self.inow <= self.imax and self.inow == self.inext:
            sys.stdout.write("#")
            sys.stdout.flush()
            if self.inow == self.imax:
                sys.stdout.write("\n")
                self.stop = time.time()
            self.inext += self.istep
    
    ###########################################################################
    def time(self):
        """
        Return the timing per step and the total time elapsed.
        """
        return self.stop - self.start, (self.stop - self.start)/float(self.imax)
    
###############################################################################
def latex(word):
    """
    Return a word with special LaTeX characters formatted,
    e.g. particle names.

    name: string to format.
    """
    if word == "->": return r"\rightarrow"
    base, par, sub, sup = word, "", "", ""

    # Handle parentheses.
    split = word.split("(")
    if len(split) > 1: base, par = split[0], "(" + "(".join(split[1:])
        
    # Handle subscripts.
    split = base.split("_")
    if len(split) > 1: base, sub = split[0], sub + "".join(split[1:])

    # Handle superscripts.
    alpha = ""
    for char in base:
        if char.isalpha(): alpha += char
        else: sup += char
    base = alpha
    sup += par[par.find(")") + 1:]
    par = par[0:par.find(")") + 1]

    # Put everything together.
    if len(base[0:base.find("bar")]) > 0: base = "\\" + base
    if "bar" in base: base = r"\bar{" + base[0:base.find("bar")] + "}"
    if sub: base += "_{" + sub + "}"
    if sup: base += "^{" + sup + "}"
    if par: base += par
    return base

###############################################################################
def logo(x = 0.87, y = 0.87, width = 0.12,
         c1 = "tomato", c2 = "cornflowerblue", c3 = "gold", gen = False):
    """
    Draw the CIMBA logo onto a plot. This requires the Matplotlib
    module. The mane of CIMBA is generated by Pythia 8 soft QCD
    events.

    x:     the lower left-hand x-position of the logo (fraction of the page).
    y:     the lower left-hand y-position of the logo (fraction of the page).
    width: width of the logo (fraction of the page).
    c1:    color of the mane.
    c2:    color of the outline.
    c3:    color of the head.
    gen:   if true, run Pythia 8 to generate a new mane.
    """
    from matplotlib import pyplot, path, patches
    r, x, y, width, lw = 1.3, float(x), float(y), float(width), 4*width
    wx, wy, ox, oy = 0.022, 0.033, 50., 33.
    a = pyplot.axes([x, y, width, width], frameon = False)

    # Minimum bias mane.
    if gen:
        import pythia8
        pythia = pythia8.Pythia("", False)
        pythia.readString("SoftQCD:all = on")
        pythia.readString("Print:quiet = on")
        pythia.readString("HadronLevel:Decay = off")
        pythia.init()
        pythia.next()
        prts = []
        for prt in pythia.event:
            pt = prt.pT()/r
            if pt:  prts += [(prt.px()/pt, prt.py()/pt)]
    else: prts = [
            (-0.72, -1.09), ( 1.15,  0.61), ( 0.11,  1.30), (-0.48,  1.21),
            (-1.29, -0.16), (-1.30,  0.04), (-0.12, -1.29), ( 1.28,  0.25),
            (-0.05, -1.30), (-0.50, -1.20), (-0.01, -1.30), (-0.54, -1.18),
            (-1.29, -0.16), (-1.29,  0.14), ( 0.05,  1.30), (-0.75,  1.06),
            (-1.29,  0.14), (-0.83,  1.00), (-0.87,  0.96), (-1.25, -0.35),
            (-1.29,  0.12), (-1.30, -0.07), (-1.06,  0.75), (-1.30,  0.07),
            (-1.25,  0.36), (-0.69,  1.10), (-1.11,  0.68), (-1.29,  0.14),
            (-1.30,  0.03), ( 1.18, -0.53), ( 1.22, -0.44), ( 1.19,  0.53),
            (-0.92, -0.92), ( 1.04, -0.77), ( 1.00, -0.83), ( 1.17, -0.57),
            ( 1.23,  0.43), ( 1.07, -0.73), ( 1.11,  0.67), (-0.40, -1.24),
            ( 1.15, -0.61), ( 1.22, -0.44), (-0.59, -1.16), ( 1.15, -0.61),
            ( 0.90, -0.94), ( 1.29,  0.19), ( 0.95, -0.88), ( 1.14,  0.63),
            ( 0.61, -1.15), (-1.29,  0.18), ( 0.19, -1.29), ( 0.29, -1.27),
            ( 1.29,  0.13), ( 0.06, -1.30), (-1.28,  0.20), (-1.30, -0.02),
            (-1.29, -0.14), (-1.27, -0.27), (-1.27, -0.28), ( 0.22, -1.28),
            (-1.27, -0.28), (-0.76,  1.05), (-1.23,  0.42), ( 1.17, -0.57),
            (-0.42,  1.23), (-0.54,  1.18), ( 1.29,  0.15), (-0.92, -0.91),
            (-0.32,  1.26), (-1.03, -0.80), ( 0.91,  0.93), (-1.29,  0.15),
            (-0.93,  0.91), ( 1.30,  0.06), (-0.83,  1.00), (-0.53,  1.19),
            (-1.09,  0.71), ( 0.21,  1.28), (-1.30,  0.04), (-0.98,  0.85),
            (-1.29,  0.18), (-0.77, -1.05), (-1.30, -0.04), ( 0.03,  1.30),
            ( 0.13,  1.29), ( 0.11,  1.30), (-0.27,  1.27), (-1.29,  0.14),
            ( 0.90, -0.94), (-1.30,  0.01), ( 1.30, -0.00), (-0.58,  1.16),
            ( 0.45,  1.22), ( 0.08,  1.30), ( 0.23,  1.28), (-0.70,  1.10),
            (-0.02,  1.30), ( 0.15,  1.29), (-0.03,  1.30), ( 0.26,  1.27),
            (-1.20,  0.50), (-0.01, -1.30), ( 0.81,  1.02), ( 0.98, -0.86),
            ( 1.03, -0.80), ( 0.10, -1.30), (-0.38, -1.24), ( 0.12, -1.29),
            ( 1.23,  0.41), (-1.26,  0.31), (-1.18,  0.55), ( 1.29, -0.18),
            (-1.21,  0.48), ( 1.14, -0.63), (-0.90,  0.94), ( 1.30,  0.06),
            ( 0.26,  1.27), ( 0.59,  1.16), ( 0.44,  1.22), ( 1.25,  0.34),
            ( 1.29, -0.15), ( 0.93, -0.91), ( 0.20, -1.29), ( 1.09, -0.71),
            ( 0.40, -1.24), (-0.10, -1.30), (-0.66, -1.12), ( 0.46, -1.21),
            ( 1.30,  0.06), (-1.11,  0.68), (-0.96, -0.88), (-0.04, -1.30),
            ( 0.51, -1.19), ( 0.29, -1.27), (-0.19, -1.29), (-0.59, -1.16),
            (-1.09, -0.71), (-1.17,  0.57), (-0.95,  0.89), (-0.29, -1.27),
            ( 1.21, -0.46), ( 1.24, -0.40), (-0.71, -1.09), ( 0.03, -1.30),
            ( 0.58, -1.16), ( 0.16, -1.29), (-0.24, -1.28), ( 0.25, -1.28),
            ( 1.28, -0.23), ( 0.14, -1.29), (-0.13, -1.29), ( 0.23, -1.28),
            (-0.69, -1.10), ( 0.72, -1.08), (-0.96, -0.88), (-0.97,  0.86),
            ( 0.71, -1.09), ( 0.57, -1.17), (-0.89,  0.94), ( 0.34, -1.26),
            (-0.37,  1.25), ( 1.17, -0.57), (-0.86,  0.97), ( 1.29,  0.18),
            ( 1.01,  0.81), ( 1.13,  0.64), (-0.96, -0.88), ( 0.75,  1.06),
            (-1.20, -0.50), ( 0.06, -1.30), (-0.04,  1.30), (-1.17, -0.56),
            ( 1.30,  0.03), (-0.47,  1.21), (-0.47, -1.21), (-1.30, -0.03),
            ( 1.21,  0.47), (-1.10, -0.70), ( 1.06,  0.75), (-1.24,  0.38),
            (-0.17,  1.29), ( 1.08,  0.73), (-1.21, -0.47), (-0.80,  1.02),
            ( 0.33, -1.26), ( 1.18, -0.53), (-0.51, -1.20), ( 0.57,  1.17),
            ( 1.30, -0.08), ( 0.43, -1.23), ( 0.98,  0.85), (-0.93,  0.91),
            ( 1.13,  0.65), (-1.27,  0.27), ( 1.18,  0.55), ( 0.81, -1.01),
            (-0.41, -1.23), ( 0.05, -1.30), ( 1.14,  0.62), (-0.96, -0.87),
            ( 0.44, -1.22), ( 0.56,  1.18), (-0.88, -0.96), ( 0.52,  1.19),
            (-0.92, -0.92), ( 1.30, -0.03), ( 1.07,  0.73), (-0.93, -0.91),
            (-0.15,  1.29), ( 0.05,  1.30), ( 0.14, -1.29), ( 0.85,  0.98)
    ]
    for x, y in prts:
        prt = [(0, 0), (x, y)]
        prt = path.Path(prt, [path.Path.MOVETO, path.Path.LINETO])
        a.add_patch(patches.PathPatch(prt, facecolor = "none",
                                      edgecolor = c1, lw = lw))
    a.add_patch(patches.Circle((0, 0), r, facecolor = "none",
                               edgecolor = c2, lw = 3*lw))
        
    # Curves for nose and head.
    nose = [
        (26.36, 33.55), (28.61, 32.91), (37.15, 29.35), (37.44, 28.93),
        (37.60, 28.70), (38.25, 25.85), (38.88, 22.59), (40.21, 15.76),
        (40.52, 15.11), (43.09, 13.91), (45.13, 12.95), (50.76, 11.89),
        (53.69, 11.91), (58.66, 11.95), (60.09, 13.82), (60.77, 21.16),
        (61.13, 25.09), (61.89, 27.43), (62.98, 27.94), (63.27, 28.07),
        (64.72, 28.42), (66.22, 28.70), (67.71, 28.98), (69.76, 29.54),
        (70.78, 29.93)
    ]
    nose = [((x - ox)*wx, (y - oy)*wy) for x, y in nose]
    head = [
        (94.43, 30.93), (95.88, 32.09), (96.22, 32.53), (97.17, 34.50),
        (98.15, 36.53), (98.25, 36.98), (98.24, 39.01), (98.22, 41.71),
        (96.67, 46.51), (95.15, 48.58), (90.67, 54.68), (81.51, 56.53),
        (76.36, 52.36), (75.14, 51.37), (74.77, 50.84), (73.22, 47.85),
        (71.96, 45.40), (71.31, 44.40), (70.89, 44.23), (70.43, 44.03),
        (69.45, 44.36), (66.07, 45.82), (54.64, 50.75), (45.69, 52.23),
        (38.05, 50.44), (36.61, 50.10), (33.74, 49.42), (31.67, 48.93),
        (28.32, 48.14), (27.81, 48.10), (27.14, 48.58), (25.30, 49.92),
        (20.44, 53.00), (19.31, 53.54), (17.39, 54.46), (14.95, 54.89),
        (13.19, 54.62), ( 9.90, 54.11), ( 6.31, 51.04), ( 4.00, 46.77),
        (-0.62, 38.21), ( 1.95, 31.64), (11.14, 28.51), (12.57, 28.02),
        (14.95, 27.12), (16.44, 26.49), (19.18, 25.18), (24.71, 21.43),
        (25.55, 19.43), (27.30, 14.78), (28.05,  8.72), (39.21,  4.08),
        (41.80,  3.00), (48.88,  1.94), (52.40,  1.76), (62.00,  1.95),
        (65.69,  4.24), (68.50,  7.11), (71.50, 10.17), (72.85, 13.85),
        (73.72, 17.48), (77.24, 27.86), (87.55, 25.53), (94.43, 30.93)
    ]
    head = [((x - ox)*wx, (y - oy)*wy) for x, y in head]
    nose = path.Path(nose, [path.Path.MOVETO] +
                     [path.Path.CURVE4]*(len(nose) - 1))
    head = path.Path(head, [path.Path.MOVETO] +
                     [path.Path.CURVE4]*(len(head) - 1))
    a.add_patch(patches.PathPatch(head, facecolor = c3, edgecolor = c2,
                                  lw = 3*lw))
    a.add_patch(patches.PathPatch(nose, facecolor = "none", edgecolor = c2,
                                  lw = 3*lw))

    # Set limits.
    a.set_xlim([-r*1.05, r*1.05])
    a.set_ylim([-r*1.05, r*1.05])
    a.set_xticks([])
    a.set_yticks([])
