# CIMBA is licensed under the GNU GPL version 2 or later.
# Copyright (C) 2020 Philip Ilten.
"""
CIMBA is the companion software package to the paper 'CIMBA: fast
Monte Carlo generation using cubic interpolation' and is a framework
for generating single particle distributions from minimum bias
events. The following directory structure is in place:

data:  interpolation grids for different beam configurations are provided 
       here.
grids: code is provided here to generate custom grids.

Example usage is as follows:
'''
# Load the module.
import cimba

# Create the random number generator.
import random
rng = random.Random()

# Load the grid.
grid = cimba.grid('data/pp14TeV.pkl')

# Create the particle gun.
pgun = cimba.ParticleGun(grid, "all/211", rng.random)

# Generate a pi+.
px, py, pz, weight = pgun()
'''

More detailed examples with explanations are provided in the
'examples' directory and further documentation is provided per
sub-module and class.
"""
from .interpolate import (
    Histogram, PDF, CDF, InverseCDF,
    Sample1D, Sample2D, SampleTH1, SampleTH2, SampleTH2s)
from .generate import grid, ParticleGun, PythiaDecay
